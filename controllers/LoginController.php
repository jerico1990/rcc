<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
     public function beforeAction($action)
     {
         if ($action->id == 'error'){
             $this->layout = 'login';
           }
         return parent::beforeAction($action);
     }

     public function actions()
     {
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
     }
    //
    // public function actions()
    // {
    //     return [
    //         'error' => [
    //             'class' => 'yii\web\ErrorAction',
    //         ],
    //         'captcha' => [
    //             'class' => 'yii\captcha\CaptchaAction',
    //             'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
    //         ],
    //     ];
    // }

    /**
     * Displays homepage.
     *
     * @return string
     */
     public function actionIndex()
     {
        //return $this->redirect(['site/index']);
		$this->layout='login';
		$model = new LoginForm();

		if (!Yii::$app->user->isGuest) {
				return $this->redirect(['panel/index']);
		}

		if ($model->load(Yii::$app->request->post()) && $model->login()) {

			return $this->redirect(['panel/index']);
		}
		/*
		if (LoginController::isLogin()){
			echo "Aaa";
		}else{*/
		//  LoginController::getUserAuth();
			return $this->render('index', [
				'model' => $model,
			]);
        //}
        
     }

     public function actionLogout()
     {
         Yii::$app->user->logout();
         return $this->goHome();
     }

}
