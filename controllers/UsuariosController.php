<?php

namespace app\controllers;

use Yii;
use app\models\Fur;
use app\models\FurSearch;
use app\models\P2Persona;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * FurController implements the CRUD actions for Fur model.
 */
class UsuariosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fur models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $this->layout='intranet';
        return $this->render('index');
    }


    public function actionGetListaUsuarios(){
        if($_POST){
            $usuarios = (new \yii\db\Query());

            if(Yii::$app->user->identity->badministrador == 1){
                $usuarios = $usuarios
                    ->from('p2_persona');
            }else{
                $usuarios = $usuarios
                    ->from('p2_persona');
            }

            if(isset($_POST['nombres']) && $_POST['nombres']!=""){
                $usuarios = $usuarios->andWhere(['like', 'p2_persona.cnombre', $_POST['nombres']]);
            }

            if(isset($_POST['usuario']) && $_POST['usuario']!=""){
                $usuarios = $usuarios->andWhere(['like', 'p2_persona.ccorreo',  $_POST['usuario']]);
            }
            
            //$usuarios = $usuarios->andWhere(['=', 'p2_persona.bestado', 1]);
            $usuarios = $usuarios->all();
            
            return json_encode(['success'=>true,'data'=>$usuarios]);
        }
    }

    public function actionCreate()
    {
        $this->layout='intranet';
        $request = Yii::$app->request;
        $model = new P2Persona();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new P1Proyecto",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new P1Proyecto",
                    'content'=>'<span class="text-success">Create P1Proyecto success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new P1Proyecto",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())  && $model->validate()) {
                $model->ccontra=Yii::$app->getSecurity()->generatePasswordHash($model->contrasena);
                $model->save();
                return $this->refresh();
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing P1Proyecto model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout='intranet';
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->contrasena = $model->ccontra;

        //$model->est_basic_fase = 'On';
        //$model->est_basic_fase = ($model->est_basic_fase==1)?'on':'off';
        /*$model->region_lista = $ubigeo->UbigeoArrayHelper(2);
        $model->provincia_lista = $ubigeo->UbigeoArrayHelper(4);
        $model->distrito_lista = $ubigeo->UbigeoArrayHelper(6);*/
        
        if($request->isAjax){
            /*  
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update P1Proyecto #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "P1Proyecto #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update P1Proyecto #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->badministrador = ($model->badministrador=='1')?1:0;
                $model->bmonitor = ($model->bmonitor=='1')?1:0;
                $model->bejecutoras = ($model->bejecutoras=='1')?1:0;
                if($model->contrasena != $model->ccontra){
                    $model->ccontra = Yii::$app->getSecurity()->generatePasswordHash($model->contrasena);
                }
                
                $model->save();
                return $this->refresh();
                //return $this->redirect(['view', 'id' => $model->proyecto_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    protected function findModel($id)
    {
        if (($model = P2Persona::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
