<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class ProyectosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='intranet';
        return $this->render('index');
    }


    public function actionGetListaProyectos(){
        if($_POST){
            $proyectos = (new \yii\db\Query())
                    ->select('p1_proyecto.idproyec,p1_proyecto.cnombre,distrito.nombre ndistrito, provincia.nombre nprovincia, region.nombre nregion')
                    ->from('p1_proyecto')
                    ->innerJoin('ubigeo distrito','distrito.id=p1_proyecto.idubigeo and LENGTH(distrito.id)=6')
                    ->innerJoin('ubigeo provincia','provincia.id=substring(p1_proyecto.idubigeo,1,4) and LENGTH(provincia.id)=4')
                    ->innerJoin('ubigeo region','region.id=substring(p1_proyecto.idubigeo,1,2) and LENGTH(region.id)=2')
                    ->all();
            return json_encode(['success'=>true,'data'=>$proyectos]);
        }
    }

    public function actionGetListaProyectosMarkers(){
        if($_POST){
            $markers = (new \yii\db\Query())
                    ->select('p1_proyecto.idproyec,p1_proyecto.latitud,p1_proyecto.longitud')
                    ->from('p1_proyecto')
                    ->where('latitud is not null')
                    ->all();
            return json_encode(['success'=>true,'data'=>$markers]);
        }
    }

}
