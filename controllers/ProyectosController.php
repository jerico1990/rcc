<?php

namespace app\controllers;

use Yii;
use app\models\Maestro;
use app\models\Ubigeo;
use app\models\Asignacion;
use app\models\P1Proyecto;
use app\models\GestionPresupuestal;

use app\models\P1ProyectoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
/**
 * ProyectosController implements the CRUD actions for P1Proyecto model.
 */
class ProyectosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all P1Proyecto models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $this->layout = 'intranet';
        return $this->render('index');
    }


    /**
     * Displays a single P1Proyecto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "P1Proyecto #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new P1Proyecto model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new P1Proyecto();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new P1Proyecto",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new P1Proyecto",
                    'content'=>'<span class="text-success">Create P1Proyecto success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new P1Proyecto",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->proyecto_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing P1Proyecto model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout='intranet';
        $request = Yii::$app->request;
        $maestro = new Maestro;
        $model = $this->findModel($id);
        $model->monto_base = number_format($model->monto_base, 2, '.', ',');
        $model->lista_modalidad_ejecucion_estudio_basico = $maestro->MaestroArrayHelper(1);
        $model->lista_modalidad_ejecucion_obra = $maestro->MaestroArrayHelper(8);
        $model->lista_gestion_presupuestal_fase_1 = $maestro->MaestroArrayGestionPresupuestal(4,1,$model->proyecto_id);
        $model->lista_proceso_seleccion_fase_1 = $maestro->MaestroArrayProcesoSeleccion(12,1,$model->proyecto_id);
        $model->lista_expediente_tecnico_fase_1 = $maestro->MaestroArrayExpedienteTecnico(17,1,$model->proyecto_id);

        //$model->est_basic_fase = 'On';
        //$model->est_basic_fase = ($model->est_basic_fase==1)?'on':'off';
        /*$model->region_lista = $ubigeo->UbigeoArrayHelper(2);
        $model->provincia_lista = $ubigeo->UbigeoArrayHelper(4);
        $model->distrito_lista = $ubigeo->UbigeoArrayHelper(6);*/
        
        if($request->isAjax){
            /*  
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update P1Proyecto #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "P1Proyecto #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update P1Proyecto #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                $model->monto_base=str_replace(',', '', $model->monto_base);
                
                $model->estudio_basico_fase = ($model->estudio_basico_fase=='1')?1:0;
                $model->ejecucion_fase = ($model->ejecucion_fase=='1')?1:0;
                
                if($model->asignar_proyecto_personas && count($model->asignar_proyecto_personas)>0){
                    
                    Asignacion::deleteAll('p1_proyecto_id=:p1_proyecto_id',[':p1_proyecto_id'=>$model->proyecto_id]);
                    foreach($model->asignar_proyecto_personas as $key){
                        $asignacion = new Asignacion;
                        $asignacion->p1_proyecto_id=$model->proyecto_id;
                        $asignacion->p2_persona_id=$key;
                        $asignacion->save();
                    }
                }
                $model->save();
                return $this->refresh();
                //return $this->redirect(['view', 'id' => $model->proyecto_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing P1Proyecto model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing P1Proyecto model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the P1Proyecto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return P1Proyecto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = P1Proyecto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetListaProyectos(){
        if($_POST){
            if(Yii::$app->user->identity->badministrador == 1){
                $proyectos = (new \yii\db\Query())
                    ->select('p1_proyecto.proyecto_id,p1_proyecto.intervencion,distrito.nombre ndistrito, provincia.nombre nprovincia, region.nombre nregion')
                    ->from('p1_proyecto')
                    ->leftJoin('ubigeo distrito','distrito.id=p1_proyecto.ubigeo_id and LENGTH(distrito.id)=6')
                    ->leftJoin('ubigeo provincia','provincia.id=substring(p1_proyecto.ubigeo_id,1,4) and LENGTH(provincia.id)=4')
                    ->leftJoin('ubigeo region','region.id=substring(p1_proyecto.ubigeo_id,1,2) and LENGTH(region.id)=2')
                    ->all();
            }else{
                $proyectos = (new \yii\db\Query())
                    ->select('p1_proyecto.proyecto_id,p1_proyecto.intervencion,distrito.nombre ndistrito, provincia.nombre nprovincia, region.nombre nregion')
                    ->from('p1_proyecto')
                    ->innerJoin('asignacion','asignacion.p1_proyecto_id=p1_proyecto.proyecto_id')
                    ->leftJoin('ubigeo distrito','distrito.id=p1_proyecto.ubigeo_id and LENGTH(distrito.id)=6')
                    ->leftJoin('ubigeo provincia','provincia.id=substring(p1_proyecto.ubigeo_id,1,4) and LENGTH(provincia.id)=4')
                    ->leftJoin('ubigeo region','region.id=substring(p1_proyecto.ubigeo_id,1,2) and LENGTH(region.id)=2')
                    ->where('asignacion.p2_persona_id=:p2_persona_id',[':p2_persona_id'=>Yii::$app->user->identity->id])
                    ->all();
            }
            
            return json_encode(['success'=>true,'data'=>$proyectos]);
        }
    }

    public function actionGetListaProyectosMarkers(){
        if($_POST){
            $markers = (new \yii\db\Query())
                    ->select('p1_proyecto.proyecto_id,p1_proyecto.latitud,p1_proyecto.longitud')
                    ->from('p1_proyecto')
                    ->where('latitud is not null')
                    ->all();
            return json_encode(['success'=>true,'data'=>$markers]);
        }
    }

    public function actionGetListaPersonas(){
        if($_POST){
            $personas = (new \yii\db\Query())
                    ->from('p2_persona')
                    ->where('beditor=:beditor',[':beditor'=>0])
                    ->all();
            return json_encode(['success'=>true,'data'=>$personas]);
        }
    }

    public function actionGetListaAsignacionPersonasProyecto(){
        if($_POST){
            $asignaciones = (new \yii\db\Query())
                    ->from('asignacion')
                    ->where('p1_proyecto_id=:p1_proyecto_id',[':p1_proyecto_id'=>$_POST['proyecto_id']])
                    ->all();
            return json_encode(['success'=>true,'data'=>$asignaciones]);
        }
    }

    public function actionCrearGestionPresupuestal($proyecto_id=null){
        $this->layout="vacio";
        $request = Yii::$app->request;
        $model = new GestionPresupuestal;
        if($request->isAjax){
            
            if ($model->load($request->post())) {
                $model->save();
                return json_encode(['success'=>true,'action'=>'Create']);
                //return $this->redirect(['view', 'id' => $model->campo_id]);
            } else {
                return $this->render('_form_gestion_presupuestal', [
                    'model' => $model,
                ]);
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                //$model->usuario_id = \Yii::$app->user->id;
                $model->save();
                return $this->redirect(['campo/index']);
            } else {
                return $this->render('_form_gestion_presupuestal', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionCargarEstadosFase2($proyecto_id=null,$fase_id=null,$modalidad_id=null){
        $this->layout='vacio';
        $model = $this->findModel($proyecto_id);
        $maestro = new Maestro;
        if($modalidad_id==9){
            $model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArrayGestionPresupuestal(17,2,$model->proyecto_id);
            $model->lista_proceso_seleccion_fase_2 = $maestro->MaestroArrayProcesoSeleccion(17,2,$model->proyecto_id);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArrayExpedienteTecnico(17,2,$model->proyecto_id);
            $model->lista_obra_fase_2 = $maestro->MaestroArrayObra(17,2,$model->proyecto_id);
            return $this->render('_form_cof',['model'=>$model]);
        }elseif($modalidad_id==10){
            $model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArrayGestionPresupuestal(17,2,$model->proyecto_id);
            $model->lista_operatividad_ner_fase_2 = $maestro->MaestroArrayOperatividadNER(17,2,$model->proyecto_id);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArrayExpedienteTecnico(17,2,$model->proyecto_id);
            $model->lista_obra_fase_2 = $maestro->MaestroArrayObra(17,2,$model->proyecto_id);
            return $this->render('_form_ner',['model'=>$model]);
        }elseif($modalidad_id==11){
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArrayExpedienteTecnico(17,2,$model->proyecto_id);
            $model->lista_obra_fase_2 = $maestro->MaestroArrayObra(17,2,$model->proyecto_id);
            return $this->render('_form_clasica',['model'=>$model]);
        }

    }
}
