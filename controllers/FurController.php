<?php

namespace app\controllers;

use Yii;
use app\models\Fur;
use app\models\FurSearch;
use app\models\Maestro;
use app\models\P1Proyecto;
use app\models\SubEtapa;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * FurController implements the CRUD actions for Fur model.
 */
class FurController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fur models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $this->layout='intranet';
        //return $this->redirect('http://google.com?'.http_build_query(['param' => 12]));


        return $this->render('index');
    }


    /**
     * Displays a single Fur model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Fur #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Fur model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $this->layout='intranet';
        $maestro = new Maestro;
        $request = Yii::$app->request;
        $model = new Fur();  
        $model->lista_modalidad_ejecucion_estudio_basico = $maestro->MaestroArrayHelper(1);
        $model->lista_modalidad_ejecucion_obra = $maestro->MaestroArrayHelper(8);
        //$model->lista_gestion_presupuestal_fase_1 = $maestro->MaestroArrayGestionPresupuestal(4,1,$model->fur_id);
        //$model->lista_proceso_seleccion_fase_1 = $maestro->MaestroArrayProcesoSeleccion(12,1,$model->fur_id);
        //$model->lista_expediente_tecnico_fase_1 = $maestro->MaestroArrayExpedienteTecnico(17,1,$model->fur_id);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Fur",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Fur",
                    'content'=>'<span class="text-success">Create Fur success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new Fur",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->estado=1;
                $model->save();

                if($model->estudio_basico_modalidad_ejecucion_id==2){
                    $countET=count($model->et1_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et1_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et1_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et1_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et1_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=1;
                                $sub_etapa->modalidad_id=$model->estudio_basico_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=44;
                                $sub_etapa->situacion_id=$model->et1_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et1_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et1_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }elseif($model->estudio_basico_modalidad_ejecucion_id==2){
                    $countEIB=count($model->eib_situaciones);
                    if($countEIB>0){
                        for($i=0;$i<$countEIB;$i++)
                        {
                            if($model->eib_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->eib_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->eib_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->eib_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=1;
                                $sub_etapa->modalidad_id=$model->estudio_basico_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=39;
                                $sub_etapa->situacion_id=$model->eib_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->eib_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->eib_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }

                if($model->ejecucion_modalidad_ejecucion_id == 9){

                    $countGP=count($model->gp_situaciones);
                    if($countGP>0){
                        for($i=0;$i<$countGP;$i++)
                        {
                            if($model->gp_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->gp_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=4;
                                $sub_etapa->situacion_id=$model->gp_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countPS=count($model->ps_situaciones);
                    if($countPS>0){
                        for($i=0;$i<$countPS;$i++)
                        {
                            if($model->ps_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->ps_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->ps_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ps_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=12;
                                $sub_etapa->situacion_id=$model->ps_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->ps_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ps_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countET=count($model->et_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=17;
                                $sub_etapa->situacion_id=$model->et_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countO=count($model->o_situaciones);
                    if($countO>0){
                        
                        for($i=0;$i<$countO;$i++)
                        {
                            if($model->o_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->o_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=22;
                                $sub_etapa->situacion_id=$model->o_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                }
                else if($model->ejecucion_modalidad_ejecucion_id == 10){
                    $countGP=count($model->gp_situaciones);
                    if($countGP>0){
                        for($i=0;$i<$countGP;$i++)
                        {
                            if($model->gp_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->gp_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=4;
                                $sub_etapa->situacion_id=$model->gp_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countNER=count($model->ner_situaciones);
                    if($countNER>0){
                        for($i=0;$i<$countNER;$i++)
                        {
                            if($model->ner_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->ner_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->ner_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ner_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=32;
                                $sub_etapa->situacion_id=$model->ner_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->ner_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ner_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countET=count($model->et_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=17;
                                $sub_etapa->situacion_id=$model->et_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countO=count($model->o_situaciones);
                    if($countO>0){
                        
                        for($i=0;$i<$countO;$i++)
                        {
                            if($model->o_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->o_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=22;
                                $sub_etapa->situacion_id=$model->o_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }
                else if($model->ejecucion_modalidad_ejecucion_id == 11){
                    $countET=count($model->et_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=17;
                                $sub_etapa->situacion_id=$model->et_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    
                    $countO=count($model->o_situaciones);
                    if($countO>0){
                        
                        for($i=0;$i<$countO;$i++)
                        {
                            if($model->o_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->o_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=22;
                                $sub_etapa->situacion_id=$model->o_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }
                


                
                return $this->redirect(['fur/index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Fur model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $this->layout='intranet';
        $request = Yii::$app->request;
        $maestro = new Maestro;
        $model = $this->findModel($id);       
        $model->lista_modalidad_ejecucion_estudio_basico = $maestro->MaestroArrayHelper(1);
        $model->lista_modalidad_ejecucion_obra = $maestro->MaestroArrayHelper(8);
        //$model->lista_gestion_presupuestal_fase_1 = $maestro->MaestroArrayGestionPresupuestal(4,1,$model->fur_id);
        //$model->lista_proceso_seleccion_fase_1 = $maestro->MaestroArrayProcesoSeleccion(12,1,$model->fur_id);
        //$model->lista_expediente_tecnico_fase_1 = $maestro->MaestroArrayExpedienteTecnico(17,1,$model->fur_id);
        $modalidad_fase_2 = $model->ejecucion_modalidad_ejecucion_id;
        $modalidad_fase_1 = $model->estudio_basico_modalidad_ejecucion_id;
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Fur #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Fur #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Fur #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {

                if($modalidad_fase_2!=$model->ejecucion_modalidad_ejecucion_id){
                    SubEtapa::deleteAll('fur_id = :fur_id AND fase_id=:fase_id', [':fur_id' => $model->fur_id, ':fase_id' => 2]);
                }

                if($modalidad_fase_1!=$model->estudio_basico_modalidad_ejecucion_id){
                    SubEtapa::deleteAll('fur_id = :fur_id AND fase_id=:fase_id', [':fur_id' => $model->fur_id, ':fase_id' => 1]);
                }


                if($model->estudio_basico_modalidad_ejecucion_id==2){
                    $countET=count($model->et1_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et1_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et1_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et1_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et1_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=1;
                                $sub_etapa->modalidad_id=$model->estudio_basico_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=44;
                                $sub_etapa->situacion_id=$model->et1_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et1_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et1_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }elseif($model->estudio_basico_modalidad_ejecucion_id==2){
                    $countEIB=count($model->eib_situaciones);
                    if($countEIB>0){
                        for($i=0;$i<$countEIB;$i++)
                        {
                            if($model->eib_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->eib_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->eib_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->eib_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=1;
                                $sub_etapa->modalidad_id=$model->estudio_basico_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=39;
                                $sub_etapa->situacion_id=$model->eib_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->eib_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->eib_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }


                if($model->ejecucion_modalidad_ejecucion_id == 9){

                    $countGP=count($model->gp_situaciones);
                    if($countGP>0){
                        for($i=0;$i<$countGP;$i++)
                        {
                            if($model->gp_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->gp_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=4;
                                $sub_etapa->situacion_id=$model->gp_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countPS=count($model->ps_situaciones);
                    if($countPS>0){
                        for($i=0;$i<$countPS;$i++)
                        {
                            if($model->ps_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->ps_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->ps_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ps_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=12;
                                $sub_etapa->situacion_id=$model->ps_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->ps_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ps_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countET=count($model->et_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=17;
                                $sub_etapa->situacion_id=$model->et_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countO=count($model->o_situaciones);
                    if($countO>0){
                        
                        for($i=0;$i<$countO;$i++)
                        {
                            if($model->o_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->o_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=22;
                                $sub_etapa->situacion_id=$model->o_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                }
                else if($model->ejecucion_modalidad_ejecucion_id == 10){
                    $countGP=count($model->gp_situaciones);
                    if($countGP>0){
                        for($i=0;$i<$countGP;$i++)
                        {
                            if($model->gp_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->gp_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=4;
                                $sub_etapa->situacion_id=$model->gp_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->gp_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->gp_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countNER=count($model->ner_situaciones);
                    if($countNER>0){
                        for($i=0;$i<$countNER;$i++)
                        {
                            if($model->ner_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->ner_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->ner_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ner_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=32;
                                $sub_etapa->situacion_id=$model->ner_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->ner_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->ner_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countET=count($model->et_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=17;
                                $sub_etapa->situacion_id=$model->et_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }

                    $countO=count($model->o_situaciones);
                    if($countO>0){
                        
                        for($i=0;$i<$countO;$i++)
                        {
                            if($model->o_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->o_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=22;
                                $sub_etapa->situacion_id=$model->o_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }
                else if($model->ejecucion_modalidad_ejecucion_id == 11){
                    $countET=count($model->et_situaciones);
                    if($countET>0){
                        for($i=0;$i<$countET;$i++)
                        {
                            if($model->et_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->et_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=17;
                                $sub_etapa->situacion_id=$model->et_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->et_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->et_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }


                    $countO=count($model->o_situaciones);
                    if($countO>0){
                        
                        for($i=0;$i<$countO;$i++)
                        {
                            if($model->o_sub_etapas_ids[$i]!=''){
                                $sub_etapa=SubEtapa::findOne($model->o_sub_etapas_ids[$i]);
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->save();
                            }else{
                                $sub_etapa= new SubEtapa;
                                $sub_etapa->fur_id=$model->fur_id;
                                $sub_etapa->fase_id=2;
                                $sub_etapa->modalidad_id=$model->ejecucion_modalidad_ejecucion_id;
                                $sub_etapa->etapa_id=22;
                                $sub_etapa->situacion_id=$model->o_situaciones[$i];
                                $sub_etapa->fecha_programada=$model->o_fechas_programadas[$i];
                                $sub_etapa->fecha_real=$model->o_fechas_reales[$i];
                                $sub_etapa->estado=0;
                                $sub_etapa->save();
                            }
                        }
                    }
                }

                $model->save();
                return $this->refresh();
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Fur model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Fur model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Fur model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetListaFur(){
        if($_POST){
            $fur = (new \yii\db\Query());

            if(Yii::$app->user->identity->administrador == 1 || Yii::$app->user->identity->monitor == 1){
                $fur =  $fur
                        ->from('fur');
            }else{
                $fur = $fur
                        ->from('fur');
            }

            if(isset($_POST['codigo_fur']) && $_POST['codigo_fur']!=""){
                $fur = $fur->andWhere(['=', 'fur.codigo_fur', $_POST['codigo_fur']]);
            }

            if(isset($_POST['denominacion_fur']) && $_POST['denominacion_fur']!=""){
                $fur = $fur->andWhere(['like', 'fur.denominacion_fur', $_POST['denominacion_fur']]);
            }

            
            $fur = $fur->andWhere(['=', 'fur.estado', 1]);
            $fur = $fur->all();
            
            return json_encode(['success'=>true,'data'=>$fur]);
        }
    }

    public function actionCargarEtapasFase1($fur_id=null,$fase_id=null,$modalidad_id=null){
        $this->layout='vacio';
        
        if($fur_id){
            $model = $this->findModel($fur_id);
        }
        else{
            $model = new Fur;
        }
        $maestro = new Maestro;
        if($modalidad_id==2){
            //$model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArrayGestionPresupuestal(17,2,$model->fur_id);
            $model->lista_expediente_tecnico_fase_1 = $maestro->MaestroArraySubEtapas($model->fur_id,1,2,44);

            /*
            $model->lista_proceso_seleccion_fase_2 = $maestro->MaestroArrayProcesoSeleccion(17,2,$model->fur_id);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArrayExpedienteTecnico(17,2,$model->fur_id);
            $model->lista_obra_fase_2 = $maestro->MaestroArrayObra(17,2,$model->fur_id);*/
            return $this->render('_form_et',['model'=>$model]);
        }elseif($modalidad_id==3){
            $model->lista_eib_fase_1 = $maestro->MaestroArraySubEtapas($model->fur_id,1,3,39);


            return $this->render('_form_eib',['model'=>$model]);
        }

    }


    public function actionCargarEtapasFase2($fur_id=null,$fase_id=null,$modalidad_id=null){
        $this->layout='vacio';
        
        if($fur_id){
            $model = $this->findModel($fur_id);
        }
        else{
            $model = new Fur;
        }
        $maestro = new Maestro;
        if($modalidad_id==9){
            //$model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArrayGestionPresupuestal(17,2,$model->fur_id);
            $model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,9,4);
            $model->lista_proceso_seleccion_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,9,12);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,9,17);
            $model->lista_obra_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,9,22);

            /*
            $model->lista_proceso_seleccion_fase_2 = $maestro->MaestroArrayProcesoSeleccion(17,2,$model->fur_id);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArrayExpedienteTecnico(17,2,$model->fur_id);
            $model->lista_obra_fase_2 = $maestro->MaestroArrayObra(17,2,$model->fur_id);*/
            return $this->render('_form_cof',['model'=>$model]);
        }elseif($modalidad_id==10){
            $model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,10,4);
            $model->lista_operatividad_ner_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,10,32);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,10,17);
            $model->lista_obra_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,10,22);


            return $this->render('_form_ner',['model'=>$model]);
        }elseif($modalidad_id==11){
            $model->lista_gestion_presupuestal_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,11,4);
            $model->lista_expediente_tecnico_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,11,17);
            $model->lista_obra_fase_2 = $maestro->MaestroArraySubEtapas($model->fur_id,2,11,22);


            return $this->render('_form_clasica',['model'=>$model]);
        }

    }

    public function actionEliminarFur()
    {
        if($_POST){
            $model = $this->findModel($_POST['fur_id']);
            $model->estado = 0 ;
            $model->update();
            return json_encode(['success'=>true]);
        }
    }

    public function actionAsignarIntervencion(){
        if($_POST){
            $model = P1Proyecto::findOne($_POST['intervencion_id']);
            $model->fur_id=$_POST['fur_id'];
            $model->update();
            return json_encode(['success'=>true]);
        }
    }

    public function actionQuitarAsignacionIntervencion(){
        if($_POST){
            $model = P1Proyecto::findOne($_POST['intervencion_id']);
            $model->fur_id=NULL;
            $model->update();
            return json_encode(['success'=>true]);
        }
    }

    public function actionGetListaIntervenciones(){
        if($_POST){
            $intervenciones = (new \yii\db\Query())
                    ->select('p1_proyecto.codigo_arcc,p1_proyecto.intervencion,p1_proyecto.monto_base')
                    ->from('p1_proyecto')
                    ->where('p1_proyecto.fur_id=:fur_id',[':fur_id'=>$_POST['fur_id']]);
            $intervenciones = $intervenciones->all();
            
            return json_encode(['success'=>true,'data'=>$intervenciones]);
        }
    }

    public function actionActivarSubEtapa(){
        if($_POST){
            $subetapa = SubEtapa::findOne($_POST['sub_etapa_id']);
            $subetapaActual = SubEtapa::find()->where('estado=1 and fur_id=:fur_id and fase_id=:fase_id and modalidad_id=:modalidad_id and etapa_id=:etapa_id',
            [':fur_id'=>$subetapa->fur_id,':fase_id'=>$subetapa->fase_id,':modalidad_id'=>$subetapa->modalidad_id,':etapa_id'=>$subetapa->etapa_id])->one();
            if($subetapaActual){
                $subetapaActual->estado=0;
                $subetapaActual->update();
            }

            if($subetapa){
                $subetapa->estado=1;
                $subetapa->update();
            }

            return json_encode(['success'=>true]);
        }
    }


    public function actionDesactivarSubEtapa(){
        if($_POST){
            $subetapa = SubEtapa::findOne($_POST['sub_etapa_id']);
            if($subetapa){
                $subetapa->estado=2;
                $subetapa->update();
            }

            return json_encode(['success'=>true]);
        }
    }


    
}
