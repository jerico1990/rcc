<?php

namespace app\controllers;

use Yii;
use app\models\GestionPresupuestal;
use app\models\GestionPresupuestalSearch;
use app\models\Maestro;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * GestionPresupuestalController implements the CRUD actions for GestionPresupuestal model.
 */
class GestionPresupuestalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GestionPresupuestal models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new GestionPresupuestalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single GestionPresupuestal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "GestionPresupuestal #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new GestionPresupuestal model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($proyecto_id=null,$fase_id=null)
    {
        $this->layout="vacio";
        $request = Yii::$app->request;
        $maestro = new Maestro;
        $model = new GestionPresupuestal;
        $model->lista_tipos = $maestro->MaestroArrayHelper(4);
        $model->titulo = 'Registrar';
        $model->proyecto_id = $proyecto_id;
        $model->fase_id = $fase_id;
        if($request->isAjax){
            
            if ($model->load($request->post())) {

                $model->save();
                return json_encode(['success'=>true,'action'=>'Create']);
                //return $this->redirect(['view', 'id' => $model->campo_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                //$model->usuario_id = \Yii::$app->user->id;
                $model->save();
                return $this->redirect(['panel/index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
        /*
        $request = Yii::$app->request;
        $model = new GestionPresupuestal();  

        if($request->isAjax){
           
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new GestionPresupuestal",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new GestionPresupuestal",
                    'content'=>'<span class="text-success">Create GestionPresupuestal success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new GestionPresupuestal",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
           
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->gestion_presupuestal_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }*/
       
    }

    /**
     * Updates an existing GestionPresupuestal model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout="vacio";
        $request = Yii::$app->request;
        $maestro = new Maestro;
        $model = $this->findModel($id);       
        $model->lista_tipos = $maestro->MaestroArrayHelper(4);
        $model->titulo = 'Actualizar';
        if($request->isAjax){
            
            if ($model->load($request->post())) {
                $model->save();
                return json_encode(['success'=>true,'action'=>'Update']);
                //return $this->redirect(['view', 'id' => $model->campo_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                //$model->usuario_id = \Yii::$app->user->id;
                $model->save();
                return $this->redirect(['panel/index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        /*
        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update GestionPresupuestal #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "GestionPresupuestal #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update GestionPresupuestal #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
           
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->gestion_presupuestal_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }*/
    }

    /**
     * Delete an existing GestionPresupuestal model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing GestionPresupuestal model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the GestionPresupuestal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GestionPresupuestal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GestionPresupuestal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetLista(){
        if($_POST){
            $listas = (new \yii\db\Query())
                    ->select(['gestion_presupuestal.gestion_presupuestal_id,DATE_FORMAT(gestion_presupuestal.fecha_programada, "%d/%m/%Y") fecha_programada,DATE_FORMAT(gestion_presupuestal.fecha_real, "%d/%m/%Y") fecha_real ,maestro.descripcion tipo_gestion_presupuestal'])
                    ->from('gestion_presupuestal')
                    ->innerJoin('maestro','maestro.maestro_id=gestion_presupuestal.tipo_id')
                    ->where('proyecto_id=:proyecto_id and fase_id=:fase_id',[':proyecto_id'=>$_POST['proyecto_id'],':fase_id'=>$_POST['fase_id']])
                    ->all();
            return json_encode(['success'=>true,'data'=>$listas]);
        }
    }
    public function actionEliminar(){
        if($_POST){
            $this->findModel($_POST['gestion_presupuestal_id'])->delete();
            return json_encode(['success'=>true]);
        }

        return json_encode(['success'=>false]);
    }
    
}
