<!-- ================== BEGIN BASE JS ================== -->

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="login-box">
        
    <div class="login-logo">
        <a href="../../index2.html"><b>RCC</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Iniciar sesión</p>
        <?php $form = ActiveForm::begin(); ?>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Email" name="LoginForm[username]" id="username">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]" id="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <button type="submit" class="btn btn-primary">Iniciar sesión</button>
            </div>
            <div class="form-group has-feedback">
                <a href="#">Olvide contraseña</a>
            </div>
        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script type="text/javascript">
    $('#btn-ingresar').click(function(){

        var error='';
        if($.trim($('#username').val())==''){
            error=error+'Debes ingresar tu usuario <br>';
            $('#username').parent().addClass('has-error');
        }
        else {
            $('#username').parent().removeClass('has-error');
        }

        if($.trim($('#password').val())==''){
            error=error+'Debes ingresar tu contraseña <br>';
            $('#password').parent().addClass('has-error');
        }
        else {
            $('#password').parent().removeClass('has-error');
        }


        if(error!=''){

            return false;
        }else {
            return true;
        }
    });
</script>