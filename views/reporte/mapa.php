<link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css" type="text/css">
<style>
    .map {
    height: 100%;
    width: 100%;
    }
</style>
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
<script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>

<div id="map" class="map"></div>
<script type="text/javascript">

	
	  
    
	var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
	async function CargarMarkers(){
		var features = [];

		const results = await $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/proyectos/get-lista-proyectos-markers',
            method: 'POST',
            data:{_csrf:csrf},
			dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {    /*
                if(results && results.success){
                   
                    $.each(results.data, function( index, value ) {
						var marker = new ol.Feature({
							geometry: new ol.geom.Point(ol.proj.fromLonLat([value.longitud, value.latitud]))
						});

						marker.setStyle(new ol.style.Style({
							image: new ol.style.Icon( ({
								scale: 0.3,
								crossOrigin: 'anonymous',
								src: '//raw.githubusercontent.com/jonataswalker/map-utils/master/images/marker.png'
							}))
						}));

						features.push(marker);
					});
					console.log(features);
                    
                }*/
                
            },
            error:function(){

                alert('Error al realizar el proceso.');
            }
		});
		if(results && results.success){
			$.each(results.data, function( index, value ) {
				var marker = new ol.Feature({
					geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(value.longitud), parseFloat(value.latitud)]))
				});

				marker.setStyle(new ol.style.Style({
					image: new ol.style.Icon( ({
						scale: 0.3,
						crossOrigin: 'anonymous',
						src: '//raw.githubusercontent.com/jonataswalker/map-utils/master/images/marker.png'
					}))
				}));

				features.push(marker);
			});
		}

		var vectorSource = new ol.source.Vector({
			features: features
		});
		
		var vectorLayer = new ol.layer.Vector({
			source: vectorSource
		});
		return vectorLayer;
	}

	async function CargarMapa(){
		const vectorLayer = await CargarMarkers();
		console.log(vectorLayer);
		var map = new ol.Map({
			target: 'map',
			layers: [
				new ol.layer.Group({
					'title': 'Base maps',
					layers: [
						new ol.layer.Tile({
							title: 'OSM',
							type: 'base',
							visible: true,
							source: new ol.source.OSM()
						}),
					]
				}),
				vectorLayer
			],
			view: new ol.View({
				center: ol.proj.fromLonLat([-74.0000000, -9.0000000]),
				zoom: 5.7
			})
		});
	}

	CargarMapa();
</script>