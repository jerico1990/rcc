<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ubigeo */
?>
<div class="ubigeo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
