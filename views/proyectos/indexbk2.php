<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Proyectos
    <small>Intervención</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Panel</a></li>
        <li class="active">Lista de proyectos</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Listado de proyectos de intervención</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <table id="lista-proyectos" class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th data-priority="1" class="hidden-*-down">Región</th>
                            <th data-priority="2" >Provincia</th>
                            <th>Distrito</th>
                            <th>Nombre proyecto</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
    
    ListaProyectos();
    function ListaProyectos(){
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/proyectos/get-lista-proyectos',
            method: 'POST',
            data:{_csrf:csrf},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    var tabla = '';
                    $.each(results.data, function( index, value ) {
                        cnombre = value.cnombre;
                        tabla = tabla + '<tr>';
                            tabla = tabla + '<td>';
                                tabla = tabla + value.nregion ;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + value.nprovincia ;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + value.ndistrito ; 
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + ((cnombre)?cnombre.substring(0,80):'');
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + '<a href="<?= \Yii::$app->request->BaseUrl ?>/proyectos/update?id='+value.idproyec+'"><span class="btn btn-primary btn-sm fa fa-pencil"></span> </a> ' ; 
                                tabla = tabla + '<span class="btn btn-danger btn-sm fa fa-trash-o"></span>' ; 
                            tabla = tabla + '</td>' ;
                        tabla = tabla + '</tr>';
                    });
                    $('#lista-proyectos tbody').html(tabla);
                    $('#lista-proyectos').DataTable({
                        'paging'      : true,
                        'lengthChange': false,
                        'searching'   : false,
                        'ordering'    : true,
                        'info'        : true,
                        "responsive": true,
                        "columnDefs": [
                            { responsivePriority: 1, targets: 0 },
                            { responsivePriority: 2, targets: 2 }
                        ]
                    });
                }
                
            },
            error:function(){

                alert('Error al realizar el proceso.');
            }
        });
    }
</script>