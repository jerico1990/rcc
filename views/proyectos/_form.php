<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\P1Proyecto */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Proyectos
    <small><?= $model->intervencion ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Proyectos</a></li>
        <li class="active">Actualizar proyecto</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Ficha informativa</a></li>
                <li><a href="#tab_2" data-toggle="tab">Estudios básicos</a></li>
                <li><a href="#tab_3" data-toggle="tab">Ejecución</a></li>
                <li class="pull-right">
                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php } ?>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'codigo_arcc')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'intervencion')->textArea(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'monto_base')->textInput(['maxlength' => true,'disabled'=>true])->label('Monto base') ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php if(Yii::$app->user->identity->editor==1){ ?>
                    <div class="col-md-12">
                        <?= $form->field($model, 'asignar_proyecto_personas')->dropDownList([],['prompt'=>'Asignar','class'=>'form-control select2','multiple'=>true])->label('Asignar') ?>
                    </div>
                    <?php } ?>
                    
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'region_id')->dropDownList([],['prompt'=>'Seleccionar región','disabled'=>true])->label('Región') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'provincia_id')->dropDownList([],['prompt'=>'Seleccionar provincia','disabled'=>true])->label('Provincia') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'ubigeo_id')->dropDownList([],['prompt'=>'Seleccionar distrito','disabled'=>true])->label('Distrito') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'latitud')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'longitud')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_inicio_proyecto')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_fin_proyecto')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_inicio_ejecucion')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_fin_ejecucion')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'nro_hectareas')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'nro_familias')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="clearfix"></div>



                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'estudio_basico_fase')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Estudio Básico') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'codigo_fur')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'estudio_basico_modalidad_ejecucion_id')->dropDownList($model->lista_modalidad_ejecucion_estudio_basico,['prompt'=>'Seleccionar modalidad'])->label('Modalidad de ejecución') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Gestión presupuestal</h3>
                                <span class="pull-right">
                                    <!--<button data-id="<?= $model->proyecto_id ?>" class="btn btn-primary btn-agregar-gestion-presupuestal-estudio-basico" ><i class="la la-plus"></i>Agregar gestión presupuestal</button>-->
                                </span>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!--<table id="lista-gestion-presupuestal-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
                                <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Fecha programada</th>
                                            <th>Fecha real</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($model->lista_gestion_presupuestal_fase_1 as $valor){ ?>
                                            <tr> 
                                                <td><?= $valor['descripcion'] ?></td>
                                                <td><input type="date" class="form-control" value="<?= $valor['fecha_programada'] ?>" > </td>
                                                <td><input type="date" class="form-control" value="<?= $valor['fecha_real'] ?>" ></td>
                                                <td> <button class="btn btn-primary btn-finalizar" data-proyecto="<?= $model->proyecto_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Proceso de selección</h3>
                                <span class="pull-right">
                                    <!--<button data-id="<?= $model->proyecto_id ?>" class="btn btn-primary btn-agregar-proceso-seleccion-estudio-basico" ><i class="la la-plus"></i>Agregar proceso de selección</button>-->
                                </span>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!--<table id="lista-proceso-seleccion-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
                                <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Fecha programada</th>
                                            <th>Fecha real</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($model->lista_proceso_seleccion_fase_1 as $valor){ ?>
                                            <tr> 
                                                <td><?= $valor['descripcion'] ?></td>
                                                <td><input type="date" class="form-control" value="<?= $valor['fecha_programada'] ?>" > </td>
                                                <td><input type="date" class="form-control" value="<?= $valor['fecha_real'] ?>" ></td>
                                                <td> <button class="btn btn-primary btn-finalizar" data-proyecto="<?= $model->proyecto_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Expediente de técnico</h3>
                                <span class="pull-right">
                                    <!--<button data-id="<?= $model->proyecto_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
                                </span>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
                                <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Fecha programada</th>
                                            <th>Fecha real</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($model->lista_expediente_tecnico_fase_1 as $valor){ ?>
                                            <tr> 
                                                <td><?= $valor['descripcion'] ?></td>
                                                <td><input type="date" class="form-control" value="<?= $valor['fecha_programada'] ?>" > </td>
                                                <td><input type="date" class="form-control" value="<?= $valor['fecha_real'] ?>" ></td>
                                                <td> <button class="btn btn-primary btn-finalizar" data-proyecto="<?= $model->proyecto_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'ejecucion_fase')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Ejecución') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'ejecucion_modalidad_ejecucion_id')->dropDownList($model->lista_modalidad_ejecucion_obra,['prompt'=>'Seleccionar modalidad'])->label('Modalidad de ejecución') ?>
                    </div>
                    <div class="cargar_estados_fase_2" data-id="<?= $model->proyecto_id ?>"></div>
                    
                    
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    <?php ActiveForm::end(); ?>
</section>
<!-- /.content -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var ubigeo = '<?= ($model->ubigeo_id)?$model->ubigeo_id:''; ?>';
var region = (ubigeo)?ubigeo.substring(0,2):'';
var provincia = (ubigeo)?ubigeo.substring(0,4):'';
var distrito = (ubigeo)?ubigeo.substring(0,6):'';
var proyecto_id = '<?= $model->proyecto_id ?>';
Region();
async function Region(){
    region = (ubigeo)?ubigeo.substring(0,2):'';
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-regiones',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            console.log(region);
            if(results && results.success){
                var option='<option value>Seleccionar región</option>';
                //data.push({'id':'0','text':'Seleccionar región','selected':true});
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((region == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#p1proyecto-region_id').html(option);
            }
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}

$('body').on('change', '#p1proyecto-region_id', function (e) {
    e.preventDefault();
    region = $('#p1proyecto-region_id').val();
    provincia = $('#p1proyecto-provincia_id').val();
    $('#p1proyecto-ubigeo_id').html('<option value>Seleccionar distrito</option>');
    Provincia();
});


Provincia();
async function Provincia(){
    //var region = (ubigeo)?ubigeo.substring(0,2):$('#p1proyecto-region_id').val();
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-provincias',
        method: 'POST',
        data:{_csrf:csrf,region:region},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                var option = '<option value>Seleccionar provincia</option>';
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((provincia == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#p1proyecto-provincia_id').html(option);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}


$('body').on('change', '#p1proyecto-provincia_id', function (e) {
    e.preventDefault();
    provincia = $('#p1proyecto-provincia_id').val();
    distrito = $('#p1proyecto-ubigeo_id').val();
    Distrito();
});


Distrito();
function Distrito(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-distritos',
        method: 'POST',
        data:{_csrf:csrf,provincia:provincia},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //$("#p1proyecto-ubigeo_id").html('<option value>Seleccionar distrito</option>');
                //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                var option = '<option value>Seleccionar distrito</option>';
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((distrito == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#p1proyecto-ubigeo_id').html(option);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}

$('.select2').select2();
Asignacion();
async function Asignacion(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    var asignaciones =await  CargarAsignacion();
    const results = await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/proyectos/get-lista-personas',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });

    if(results && results.success){
        var option = '';
        
        $( results.data ).each(function( index ,value ) {
            var bandera = 0;
            $( asignaciones ).each(function( index2 ,value2 ) {
                if(value2.p2_persona_id == value.idperson){
                    bandera = 1;
                }
            });
            option = option + '<option value="' + value.idperson + '" '+((bandera == 1)?'selected':'')+' >' + value.cnombre + '</option>';
        });
        $('#p1proyecto-asignar_proyecto_personas').html(option);
    }
}


async function CargarAsignacion(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    var proyecto_id = '<?= $model->proyecto_id ?>';
    const asignaciones = await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/proyectos/get-lista-asignacion-personas-proyecto',
        method: 'POST',
        data:{_csrf:csrf,proyecto_id:proyecto_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });

    return asignaciones.data;
}

/* inicio de gestion presupuestal */
GestionPresupuestal(1);
function GestionPresupuestal(fase_id){
    
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/gestion-presupuestal/get-lista',
        method: 'POST',
        data:{_csrf:csrf,proyecto_id:proyecto_id,fase_id:fase_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                var tabla = '';
                $.each(results.data, function( index, value ) {
                    tabla = tabla + '<tr>';
                        tabla = tabla + '<td>';
                            tabla = tabla + value.tipo_gestion_presupuestal ;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + value.fecha_programada ;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + value.fecha_real ; 
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + '<span data-id="'+value.gestion_presupuestal_id+'" class="btn-actualizar-gestion-presupuestal-estudio-basico btn btn-primary btn-sm fa fa-pencil"></span> ' ; 
                            tabla = tabla + '<span data-id="'+value.gestion_presupuestal_id+'" class="btn-eliminar-gestion-presupuestal-estudio-basico btn btn-danger btn-sm fa fa-trash-o"></span>' ; 
                        tabla = tabla + '</td>' ;
                    tabla = tabla + '</tr>';
                });
                $('#lista-gestion-presupuestal-fase1 tbody').html(tabla);
                /*
                $('#lista-gestion-presupuestal-fase1').DataTable({
                    'paging'      : true,
                    'lengthChange': false,
                    'searching'   : false,
                    'ordering'    : true,
                    'info'        : true,
                    "responsive": true,
                    "columnDefs": [
                        { responsivePriority: 1, targets: 0 },
                        { responsivePriority: 2, targets: 2 }
                    ]
                });*/
            }
            
        },
        error:function(){

            alert('Error al realizar el proceso.');
        }
    });
}

$('body').on('click', '.btn-agregar-gestion-presupuestal-estudio-basico', function (e) {
    e.preventDefault();
    var proyecto_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/gestion-presupuestal/create?proyecto_id=' + proyecto_id + '&fase_id=1');
    $('#modal').modal('show');
});

$('body').on('click', '.btn-actualizar-gestion-presupuestal-estudio-basico', function (e) {
    e.preventDefault();
    var gestion_presupuestal_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/gestion-presupuestal/update?id=' +gestion_presupuestal_id );
    $('#modal').modal('show');
});


$('body').on('click', '.btn-grabar-gestion-presupuestal', function (e) {
    e.preventDefault();
    var form = $('#formGestionPresupuestal');
    var formData = $('#formGestionPresupuestal').serialize();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url: form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'Json',
        success: function (results) {
            $('#modal').modal('toggle');
            if(results && results.success){
                GestionPresupuestal(1);
            }
        },
        error: function () {
            alert("Error al procesar los datos");
        }
    });
});

$('body').on('click', '.btn-eliminar-gestion-presupuestal-estudio-basico', function (e) {
    e.preventDefault();
    var gestion_presupuestal_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/gestion-presupuestal/eliminar',
        method: 'POST',
        data:{_csrf:csrf,gestion_presupuestal_id:gestion_presupuestal_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                GestionPresupuestal(1);
            }
            
        },
        error:function(){

            alert('Error al realizar el proceso.');
        }
    });
});



/* inicio de proceso de seleccion */
ProcesoSeleccion(1);
function ProcesoSeleccion(fase_id){
    
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/proceso-seleccion/get-lista',
        method: 'POST',
        data:{_csrf:csrf,proyecto_id:proyecto_id,fase_id:fase_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                var tabla = '';
                $.each(results.data, function( index, value ) {
                    tabla = tabla + '<tr>';
                        tabla = tabla + '<td>';
                            tabla = tabla + value.tipo_proceso_seleccion ;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + value.fecha_programada ;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + value.fecha_real ; 
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + '<span data-id="'+value.proceso_seleccion_id+'" class="btn-actualizar-proceso-seleccion-estudio-basico btn btn-primary btn-sm fa fa-pencil"></span> ' ; 
                            tabla = tabla + '<span data-id="'+value.proceso_seleccion_id+'" class="btn-eliminar-proceso-seleccion-estudio-basico btn btn-danger btn-sm fa fa-trash-o"></span>' ; 
                        tabla = tabla + '</td>' ;
                    tabla = tabla + '</tr>';
                });
                $('#lista-proceso-seleccion-fase1 tbody').html(tabla);
            }
            
        },
        error:function(){

            alert('Error al realizar el proceso.');
        }
    });
}

$('body').on('click', '.btn-agregar-proceso-seleccion-estudio-basico', function (e) {
    e.preventDefault();
    var proyecto_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/proceso-seleccion/create?proyecto_id=' + proyecto_id + '&fase_id=1');
    $('#modal').modal('show');
});

$('body').on('click', '.btn-actualizar-proceso-seleccion-estudio-basico', function (e) {
    e.preventDefault();
    var proceso_seleccion_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/proceso-seleccion/update?id=' +proceso_seleccion_id );
    $('#modal').modal('show');
});


$('body').on('click', '.btn-grabar-proceso-seleccion', function (e) {
    e.preventDefault();
    var form = $('#formProcesoSeleccion');
    var formData = $('#formProcesoSeleccion').serialize();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url: form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'Json',
        success: function (results) {
            $('#modal').modal('toggle');
            if(results && results.success){
                ProcesoSeleccion(1);
            }
        },
        error: function () {
            alert("Error al procesar los datos");
        }
    });
});

$('body').on('click', '.btn-eliminar-proceso-seleccion-estudio-basico', function (e) {
    e.preventDefault();
    var proceso_seleccion_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/proceso-seleccion/eliminar',
        method: 'POST',
        data:{_csrf:csrf,proceso_seleccion_id:proceso_seleccion_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                ProcesoSeleccion(1);
            }
            
        },
        error:function(){

            alert('Error al realizar el proceso.');
        }
    });
});


/* inicio de expediente tecnico */
ExpedienteTecnico(1);
function ExpedienteTecnico(fase_id){
    
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/expediente-tecnico/get-lista',
        method: 'POST',
        data:{_csrf:csrf,proyecto_id:proyecto_id,fase_id:fase_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                var tabla = '';
                $.each(results.data, function( index, value ) {
                    tabla = tabla + '<tr>';
                        tabla = tabla + '<td>';
                            tabla = tabla + value.tipo_expediente_tecnico ;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + value.fecha_programada ;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + value.fecha_real ; 
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;
                            tabla = tabla + '<span data-id="'+value.expediente_tecnico_id+'" class="btn-actualizar-expediente-tecnico-estudio-basico btn btn-primary btn-sm fa fa-pencil"></span> ' ; 
                            tabla = tabla + '<span data-id="'+value.expediente_tecnico_id+'" class="btn-eliminar-expediente-tecnico-estudio-basico btn btn-danger btn-sm fa fa-trash-o"></span>' ; 
                        tabla = tabla + '</td>' ;
                    tabla = tabla + '</tr>';
                });
                $('#lista-expediente-tecnico-fase1 tbody').html(tabla);
            }
            
        },
        error:function(){

            alert('Error al realizar el proceso.');
        }
    });
}

$('body').on('click', '.btn-agregar-expediente-tecnico-estudio-basico', function (e) {
    e.preventDefault();
    var proyecto_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/expediente-tecnico/create?proyecto_id=' + proyecto_id + '&fase_id=1');
    $('#modal').modal('show');
});

$('body').on('click', '.btn-actualizar-expediente-tecnico-estudio-basico', function (e) {
    e.preventDefault();
    var expediente_tecnico_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/expediente-tecnico/update?id=' +expediente_tecnico_id );
    $('#modal').modal('show');
});


$('body').on('click', '.btn-grabar-expediente-tecnico', function (e) {
    e.preventDefault();
    var form = $('#formExpedienteTecnico');
    var formData = $('#formExpedienteTecnico').serialize();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url: form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'Json',
        success: function (results) {
            $('#modal').modal('toggle');
            if(results && results.success){
                ExpedienteTecnico(1);
            }
        },
        error: function () {
            alert("Error al procesar los datos");
        }
    });
});

$('body').on('click', '.btn-eliminar-expediente-tecnico-estudio-basico', function (e) {
    e.preventDefault();
    var expediente_tecnico_id = $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/expediente-tecnico/eliminar',
        method: 'POST',
        data:{_csrf:csrf,expediente_tecnico_id:expediente_tecnico_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                ExpedienteTecnico(1);
            }
            
        },
        error:function(){

            alert('Error al realizar el proceso.');
        }
    });
});


$('body').on('change', '#p1proyecto-ejecucion_modalidad_ejecucion_id', function (e) {
    e.preventDefault();
    var ejecucion_modalidad_ejecucion = $(this).val();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/gestion-presupuestal/create?proyecto_id=' + proyecto_id + '&fase_id=1');
    
});


$('body').on('change', '#p1proyecto-ejecucion_modalidad_ejecucion_id', function (e) {
    e.preventDefault();
    proyecto_id = $('.cargar_estados_fase_2').attr('data-id');
    modalidad_id = $(this).val();
    CargaEstados(modalidad_id,proyecto_id)
});

var proyecto_id = '<?= $model->proyecto_id ?>';
var modalidad_id = '<?= $model->ejecucion_modalidad_ejecucion_id ?>';

CargaEstados(modalidad_id,proyecto_id)
function CargaEstados(modalidad_id,proyecto_id){
    if(modalidad_id){
        $('.cargar_estados_fase_2').load('<?= \Yii::$app->request->BaseUrl ?>/proyectos/cargar-estados-fase2?proyecto_id=' + proyecto_id + '&fase_id=2&modalidad_id='+ modalidad_id +'');
    }
}

</script>
