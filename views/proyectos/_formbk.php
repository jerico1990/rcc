<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\P1Proyecto */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Proyectos
    <small>Actualizar</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Proyectos</a></li>
        <li class="active">Actualizar proyecto</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Listado de proyectos de intervención</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'idubigeo')->textInput(['maxlength' => true]) ?>
                   
                    <?= $form->field($model, 'cnombre')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'idubigeo')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'latitud')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'longitud')->textInput(['maxlength' => true]) ?>


                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php } ?>

                <?php ActiveForm::end(); ?>
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
