<div class="col-md-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Expediente de técnico</h3>
            <span class="pull-right">
                <!--<button data-id="<?= $model->proyecto_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
            </span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
            <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Fecha programada</th>
                        <th>Fecha real</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($model->lista_expediente_tecnico_fase_2 as $valor){ ?>
                        <tr> 
                            <td><?= $valor['descripcion'] ?></td>
                            <td><input type="date" class="form-control" value="<?= $valor['fecha_programada'] ?>" > </td>
                            <td><input type="date" class="form-control" value="<?= $valor['fecha_real'] ?>" ></td>
                            <td> <button class="btn btn-primary btn-finalizar" data-proyecto="<?= $model->proyecto_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<div class="clearfix"></div>
<div class="col-md-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Obra</h3>
            <span class="pull-right">
                <!--<button data-id="<?= $model->proyecto_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
            </span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
            <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Fecha programada</th>
                        <th>Fecha real</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($model->lista_obra_fase_2 as $valor){ ?>
                        <tr> 
                            <td><?= $valor['descripcion'] ?></td>
                            <td><input type="date" class="form-control" value="<?= $valor['fecha_programada'] ?>" > </td>
                            <td><input type="date" class="form-control" value="<?= $valor['fecha_real'] ?>" ></td>
                            <td> <button class="btn btn-primary btn-finalizar" data-proyecto="<?= $model->proyecto_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<div class="clearfix"></div>