<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\P1Proyecto */
?>
<div class="p1-proyecto-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'proyecto_id',
            'codigo_arcc',
            'ubigeo_id',
            'intervencion',
            'monto_base',
            'decreto_supremo',
        ],
    ]) ?>

</div>
