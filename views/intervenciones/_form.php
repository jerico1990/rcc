<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\P1Proyecto */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Intervención
    <small><?= $model->intervencion ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Intervención</a></li>
        <li class="active">Actualizar intervención</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Ficha informativa</a></li>
                <?php if(Yii::$app->user->identity->monitor!=1){ ?>
                <li class="pull-right">
                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'codigo_arcc')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'intervencion')->textArea(['maxlength' => true,'disabled'=>true])->label('Intervención') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'monto_base')->textInput(['maxlength' => true,'disabled'=>true])->label('Monto base') ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php if(Yii::$app->user->identity->monitor==1 || Yii::$app->user->identity->administrador==1){ ?>
                    <div class="col-md-12">
                        <?= $form->field($model, 'asignar_proyecto_personas')->dropDownList([],['prompt'=>'Seleccionar','class'=>'form-control'])->label('Ejecutora') ?>
                    </div>
                    <?php } ?>
                    
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'region_id')->dropDownList([],['prompt'=>'Seleccionar región','disabled'=>true])->label('Región') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'provincia_id')->dropDownList([],['prompt'=>'Seleccionar provincia','disabled'=>true])->label('Provincia') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'ubigeo_id')->dropDownList([],['prompt'=>'Seleccionar distrito','disabled'=>true])->label('Distrito') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'latitud')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'longitud')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
               
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    <?php ActiveForm::end(); ?>
</section>
<!-- /.content -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var ubigeo = '<?= ($model->ubigeo_id)?$model->ubigeo_id:''; ?>';
var region = (ubigeo)?ubigeo.substring(0,2):'';
var provincia = (ubigeo)?ubigeo.substring(0,4):'';
var distrito = (ubigeo)?ubigeo.substring(0,6):'';
var proyecto_id = '<?= $model->proyecto_id ?>';
Region();
async function Region(){
    region = (ubigeo)?ubigeo.substring(0,2):'';
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-regiones',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            console.log(region);
            if(results && results.success){
                var option='<option value>Seleccionar región</option>';
                //data.push({'id':'0','text':'Seleccionar región','selected':true});
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((region == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#p1proyecto-region_id').html(option);
            }
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}

$('body').on('change', '#p1proyecto-region_id', function (e) {
    e.preventDefault();
    region = $('#p1proyecto-region_id').val();
    provincia = $('#p1proyecto-provincia_id').val();
    $('#p1proyecto-ubigeo_id').html('<option value>Seleccionar distrito</option>');
    Provincia();
});


Provincia();
async function Provincia(){
    //var region = (ubigeo)?ubigeo.substring(0,2):$('#p1proyecto-region_id').val();
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-provincias',
        method: 'POST',
        data:{_csrf:csrf,region:region},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                var option = '<option value>Seleccionar provincia</option>';
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((provincia == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#p1proyecto-provincia_id').html(option);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}


$('body').on('change', '#p1proyecto-provincia_id', function (e) {
    e.preventDefault();
    provincia = $('#p1proyecto-provincia_id').val();
    distrito = $('#p1proyecto-ubigeo_id').val();
    Distrito();
});


Distrito();
function Distrito(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-distritos',
        method: 'POST',
        data:{_csrf:csrf,provincia:provincia},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //$("#p1proyecto-ubigeo_id").html('<option value>Seleccionar distrito</option>');
                //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                var option = '<option value>Seleccionar distrito</option>';
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((distrito == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#p1proyecto-ubigeo_id').html(option);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}

$('.select2').select2();
Asignacion();
async function Asignacion(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    var asignaciones =await  CargarAsignacion();
    const results = await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/intervenciones/get-lista-personas-ejecutoras',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });

    if(results && results.success){
        var option = '<option value>Seleccionar</option>';
        
        $( results.data ).each(function( index ,value ) {
            var bandera = 0;
            $( asignaciones ).each(function( index2 ,value2 ) {
                if(value2.p2_persona_id == value.idperson){
                    bandera = 1;
                }
            });
            option = option + '<option value="' + value.idperson + '" '+((bandera == 1)?'selected':'')+' >' + value.cnombre + '</option>';
        });
        $('#p1proyecto-asignar_proyecto_personas').html(option);
    }
}


async function CargarAsignacion(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    var proyecto_id = '<?= $model->proyecto_id ?>';
    const asignaciones = await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/intervenciones/get-lista-asignacion-personas-proyecto',
        method: 'POST',
        data:{_csrf:csrf,proyecto_id:proyecto_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });

    return asignaciones.data;
}

<?php if(Yii::$app->user->identity->administrador==1){ ?>
    $('input,select,textarea').prop('disabled','')
<?php } ?>

<?php if(Yii::$app->user->identity->monitor==1){ ?>
    $('input,select,textarea').prop('disabled','disabled')
<?php } ?>
</script>
