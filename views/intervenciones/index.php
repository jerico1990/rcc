<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Intervenciones
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Panel</a></li>
        <li class="active">Lista de intervenciones</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Listado de intervenciones</h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-4">
                        <label for="">Región</label>
                        <select name="" id="p1proyecto-region_id" class="form-control">
                            <option value>Seleccionar región</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="">Provincia</label>
                        <select name="" id="p1proyecto-provincia_id" class="form-control">
                            <option value>Seleccionar provincia</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="">Distrito</label>
                        <select name="" id="p1proyecto-ubigeo_id" class="form-control">
                            <option value>Seleccionar distrito</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="">Código ARCC</label>
                        <input name="" id="p1proyecto-codigo_arcc" class="form-control">
                    </div>

                    <div class="col-md-8">
                        <label for="">Intervención</label>
                        <input name="" id="p1proyecto-intervencion" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <?php if(Yii::$app->user->identity->administrador==1){ ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/intervenciones/create" class="btn btn-primary pull-left">Registrar</a>
                        <?php } ?>
                        <button class="btn btn-primary btn-buscar pull-right">Buscar</button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <table id="lista-intervenciones" class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th data-priority="1" class="hidden-*-down">Región</th>
                                    <th data-priority="2" >Provincia</th>
                                    <th>Distrito</th>
                                    <th>ARCC</th>
                                    <th>Nombre proyecto</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
    var region;
    var provincia;
    var distrito;
    var arcc;
    var intervencion;
    ListaIntervenciones();
    function ListaIntervenciones(){
        distrito = $('#p1proyecto-ubigeo_id').val();
        arcc = $('#p1proyecto-codigo_arcc').val();
        intervencion =  $('#p1proyecto-intervencion').val();
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/intervenciones/get-lista-intervenciones',
            method: 'POST',
            data:{_csrf:csrf,region:region,provincia:provincia,distrito:distrito,arcc:arcc,intervencion:intervencion},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    $('#lista-intervenciones').DataTable().destroy();
                    var tabla = '';
                    $.each(results.data, function( index, value ) {
                        cnombre = value.intervencion;
                        tabla = tabla + '<tr>';
                            tabla = tabla + '<td>';
                                tabla = tabla + value.nregion ;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + value.nprovincia ;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + value.ndistrito ; 
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + value.codigo_arcc;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + ((cnombre)?cnombre.substring(0,80):'');
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + '<a href="<?= \Yii::$app->request->BaseUrl ?>/intervenciones/update?id='+value.proyecto_id+'"><span class="btn btn-primary btn-sm fa fa-pencil"></span> </a> ' ; 
                                <?php if(Yii::$app->user->identity->administrador==1){ ?>
                                tabla = tabla + '<span data-id="' +value.proyecto_id + '" class="btn btn-danger btn-sm fa fa-trash-o btn-eliminar"></span>' ; 
                                <?php } ?>
                            tabla = tabla + '</td>' ;
                        tabla = tabla + '</tr>';
                    });
                    $('#lista-intervenciones tbody').html(tabla);

                    

                    $('#lista-intervenciones').DataTable({
                        'paging'      : true,
                        'lengthChange': false,
                        'searching'   : false,
                        'ordering'    : true,
                        'info'        : true,
                        "responsive": true,
                        "columnDefs": [
                            { responsivePriority: 1, targets: 0 },
                            { responsivePriority: 2, targets: 2 }
                        ]
                    });
                }
                
            },
            error:function(){

                alert('Error al realizar el proceso.');
            }
        });
    }  

    

    Region();
    async function Region(){
        await $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-regiones',
            method: 'POST',
            data:{_csrf:csrf},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    var option='<option value>Seleccionar región</option>';
                    //data.push({'id':'0','text':'Seleccionar región','selected':true});
                    $( results.data ).each(function( index ,value ) {
                        option = option + '<option value="' + value.id + '" >' + value.text + '</option>';
                    });
                    $('#p1proyecto-region_id').html(option);
                }
                
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }


    $('body').on('change', '#p1proyecto-region_id', function (e) {
        e.preventDefault();
        region = $('#p1proyecto-region_id').val();
        //provincia = $('#p1proyecto-provincia_id').val();
        //$('#p1proyecto-ubigeo_id').html('<option value>Seleccionar distrito</option>');
        Provincia();
    });

    async function Provincia(){
        //var region = (ubigeo)?ubigeo.substring(0,2):$('#p1proyecto-region_id').val();
        //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
        await $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-provincias',
            method: 'POST',
            data:{_csrf:csrf,region:region},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                    var option = '<option value>Seleccionar provincia</option>';
                    $( results.data ).each(function( index ,value ) {
                        option = option + '<option value="' + value.id + '" >' + value.text + '</option>';
                    });
                    $('#p1proyecto-provincia_id').html(option);
                }
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }


    $('body').on('change', '#p1proyecto-provincia_id', function (e) {
        e.preventDefault();
        provincia = $('#p1proyecto-provincia_id').val();
        //distrito = $('#p1proyecto-ubigeo_id').val();
        Distrito();
    });


    function Distrito(){
        //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
        //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-distritos',
            method: 'POST',
            data:{_csrf:csrf,provincia:provincia},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    //$("#p1proyecto-ubigeo_id").html('<option value>Seleccionar distrito</option>');
                    //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                    var option = '<option value>Seleccionar distrito</option>';
                    $( results.data ).each(function( index ,value ) {
                        option = option + '<option value="' + value.id + '" >' + value.text + '</option>';
                    });
                    $('#p1proyecto-ubigeo_id').html(option);
                }
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }


    $('body').on('click', '.btn-buscar', function (e) {
        e.preventDefault();
        ListaIntervenciones();
    });


    $('body').on('click', '.btn-eliminar', function (e) {
        e.preventDefault();
        var intervencion_id = $(this).attr('data-id');
        var r = confirm("¿Esta seguro de eliminar la intervención?");
        if (r == true) {
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/intervenciones/eliminar-intervencion',
                method: 'POST',
                data:{_csrf:csrf,intervencion_id:intervencion_id},
                dataType:'Json',
                beforeSend:function()
                {
                    
                },
                success:function(results)
                {   
                    if(results && results.success){
                        ListaIntervenciones();
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
        }
        
    });
</script>