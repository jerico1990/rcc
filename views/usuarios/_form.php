<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\P1Proyecto */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuario
    <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Usuario</a></li>
        <li class="active">registro</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Ficha de usuario</a></li>
                <li class="pull-right">
                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php } ?>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'cnombre')->textInput(['maxlength' => true])->label('Nombres') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'capepat')->textInput(['maxlength' => true])->label('Apellido paterno') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'capemat')->textInput(['maxlength' => true])->label('Apellido materno') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'ccorreo')->textInput(['maxlength' => true])->label('Usuario') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'contrasena')->textInput(['type'=>'password','maxlength' => true])->label('Contraseña') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'badministrador')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Administrador') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'bmonitor')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Monitor') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'bejecutoras')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Ejecutora') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'bestado')->dropDownList(['1'=>'Activo','0'=>'Inactivo'],['prompt'=>'Seleccionar Estado'])->label('Estado') ?>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
               
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    <?php ActiveForm::end(); ?>
</section>
<!-- /.content -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";

</script>
