<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuarios
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Panel</a></li>
        <li class="active">Lista de usuarios</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Listado de usuarios</h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="">Nombres y apellidos</label>
                        <input name="" id="p2persona-nombres" class="form-control">
                    </div>

                    <div class="col-md-8">
                        <label for="">Usuario</label>
                        <input name="" id="p2persona-usuario" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <a href="<?= \Yii::$app->request->BaseUrl ?>/usuarios/create" class="btn btn-primary pull-left">Registrar</a>
                        <button class="btn btn-primary btn-buscar pull-right">Buscar</button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <table id="lista-usuarios" class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nombres y apellidos</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
    var nombres;
    var usuario;
    ListaUsuarios();
    function ListaUsuarios(){
        nombres = $('#p2persona-nombres').val();
        usuario = $('#p2persona-usuario').val();
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/usuarios/get-lista-usuarios',
            method: 'POST',
            data:{_csrf:csrf,nombres:nombres,usuario:usuario},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    $('#lista-usuarios').DataTable().destroy();
                    var tabla = '';
                    $.each(results.data, function( index, value ) {
                        cnombre = (value.cnombre)?value.cnombre:'';
                        capepat = (value.capepat)?value.capepat:'';
                        capemat = (value.capemat)?value.capemat:'';
                        tabla = tabla + '<tr>';
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + cnombre + ' ' + capepat + ' '+ capemat;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + value.ccorreo;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + ((value.bestado)?'Activo':'Inactivo');
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + '<a href="<?= \Yii::$app->request->BaseUrl ?>/usuarios/update?id='+value.idperson+'"><span class="btn btn-primary btn-sm fa fa-pencil"></span> </a> ' ; 
                                <?php if(Yii::$app->user->identity->administrador==1){ ?>
                                tabla = tabla + '<span data-id="' +value.idperson + '" class="btn btn-danger btn-sm fa fa-trash-o btn-eliminar"></span>' ; 
                                <?php } ?>
                            tabla = tabla + '</td>' ;
                        tabla = tabla + '</tr>';
                    });
                    $('#lista-usuarios tbody').html(tabla);

                    

                    $('#lista-usuarios').DataTable({
                        'paging'      : true,
                        'lengthChange': false,
                        'searching'   : false,
                        'ordering'    : true,
                        'info'        : true,
                        "responsive": true,
                        "columnDefs": [
                            { responsivePriority: 1, targets: 0 },
                            { responsivePriority: 2, targets: 2 }
                        ]
                    });
                }
                
            },
            error:function(){

                alert('Error al realizar el proceso.');
            }
        });
    }  

    


    $('body').on('click', '.btn-buscar', function (e) {
        e.preventDefault();
        ListaUsuarios();
    });


    $('body').on('click', '.btn-eliminar', function (e) {
        e.preventDefault();
        var intervencion_id = $(this).attr('data-id');
        var r = confirm("¿Esta seguro de eliminar la intervención?");
        if (r == true) {
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/intervenciones/eliminar-intervencion',
                method: 'POST',
                data:{_csrf:csrf,intervencion_id:intervencion_id},
                dataType:'Json',
                beforeSend:function()
                {
                    
                },
                success:function(results)
                {   
                    if(results && results.success){
                        ListaIntervenciones();
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
        }
        
    });
</script>