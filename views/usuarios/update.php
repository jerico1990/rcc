<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\P1Proyecto */
?>
<div class="p1-proyecto-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
