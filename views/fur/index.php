<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Lista FUR
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Panel</a></li>
        <li class="active">Lista FUR</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Listado FUR</h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <div class="col-md-4">
                        <label for="">Código FUR</label>
                        <input name="" id="fur-codigo_fur" class="form-control">
                    </div>

                    <div class="col-md-8">
                        <label for="">Denominación FUR</label>
                        <input name="" id="fur-denominacion_fur" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <?php if(Yii::$app->user->identity->administrador==1 || Yii::$app->user->identity->ejecutoras==1){ ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/fur/create" class="btn btn-primary pull-left">Registrar</a>
                        <?php } ?>
                        <button class="btn btn-primary btn-buscar pull-right">Buscar</button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <table id="lista-fur" class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Código FUR</th>
                                    <th>Denominación FUR</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
    ListaFur();
    function ListaFur(){
        codigo_fur = $('#fur-codigo_fur').val();
        denominacion_fur = $('#fur-denominacion_fur').val();
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/fur/get-lista-fur',
            method: 'POST',
            data:{_csrf:csrf,codigo_fur:codigo_fur,denominacion_fur:denominacion_fur},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    $('#lista-fur').DataTable().destroy();
                    var tabla = '';
                    $.each(results.data, function( index, value ) {
                        tabla = tabla + '<tr>';
                           
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + value.codigo_fur;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;   
                                tabla = tabla + value.denominacion_fur;
                            tabla = tabla + '</td>' ;
                            tabla = tabla + '<td>' ;
                                tabla = tabla + '<a href="<?= \Yii::$app->request->BaseUrl ?>/fur/update?id='+value.fur_id+'"><span class="btn btn-primary btn-sm fa fa-pencil"></span> </a> ' ; 
                                <?php if(Yii::$app->user->identity->administrador==1 || Yii::$app->user->identity->ejecutoras==1){ ?>
                                tabla = tabla + '<span data-id="' +value.fur_id + '" class="btn btn-danger btn-sm fa fa-trash-o btn-eliminar"></span>' ; 
                                <?php } ?>
                            tabla = tabla + '</td>' ;
                        tabla = tabla + '</tr>';
                    });
                    $('#lista-fur tbody').html(tabla);

                    

                    $('#lista-fur').DataTable({
                        'paging'      : true,
                        'lengthChange': false,
                        'searching'   : false,
                        'ordering'    : true,
                        'info'        : true,
                        "responsive": true,
                        "columnDefs": [
                            { responsivePriority: 1, targets: 0 },
                            { responsivePriority: 2, targets: 2 }
                        ]
                    });
                }
                
            },
            error:function(){

                alert('Error al realizar el proceso.');
            }
        });
    }  

    



    $('body').on('click', '.btn-buscar', function (e) {
        e.preventDefault();
        ListaFur();
    });


    $('body').on('click', '.btn-eliminar', function (e) {
        e.preventDefault();
        var fur_id = $(this).attr('data-id');
        var r = confirm("¿Esta seguro de eliminar el registro FUR?");
        if (r == true) {
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/fur/eliminar-fur',
                method: 'POST',
                data:{_csrf:csrf,fur_id:fur_id},
                dataType:'Json',
                beforeSend:function()
                {
                    
                },
                success:function(results)
                {   
                    if(results && results.success){
                        ListaFur();
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
        }
        
    });
</script>