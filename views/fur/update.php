<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fur */
?>
<div class="fur-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
