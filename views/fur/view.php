<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fur */
?>
<div class="fur-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fur_id',
            'codigo_fur',
            'estudio_basico_fase',
            'ejecucion_fase',
            'estudio_basico_modalidad_ejecucion_id',
            'ejecucion_modalidad_ejecucion_id',
            'nro_hectareas',
            'nro_familias',
            'fecha_inicio_proyecto',
            'fecha_inicio_ejecucion',
            'fecha_fin_ejecucion',
            'fecha_fin_proyecto',
            'finalidad:ntext',
        ],
    ]) ?>

</div>
