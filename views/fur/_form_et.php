
<div class="col-md-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Expediente de técnico</h3>
            <span class="pull-right">
                <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
            </span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
            <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Fecha programada</th>
                        <th>Fecha real</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($model->lista_expediente_tecnico_fase_1 as $valor){ ?>
                        <input type="hidden" name="Fur[et1_sub_etapas_ids][]" value="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>">
                        <input type="hidden" name="Fur[et1_situaciones][]" value="<?= $valor['maestro_id'] ?>">
                        <tr> 
                            <td><?= $valor['descripcion'] ?></td>
                            <td><input type="date" name="Fur[et1_fechas_programadas][]" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                            <td><input type="date" name="Fur[et1_fechas_reales][]" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                            <?php if($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==0) { ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php }elseif($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==1){ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-danger btn-desactivar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Desactivar </button> </td>
                            <?php }else{ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<div class="clearfix"></div>