<div class="col-md-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Gestión presupuestal</h3>
            <span class="pull-right">
                <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
            </span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
            <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Fecha programada</th>
                        <th>Fecha real</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($model->lista_gestion_presupuestal_fase_2 as $valor){ ?>
                        <input type="hidden" name="Fur[gp_sub_etapas_ids][]" value="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>">
                        <input type="hidden" name="Fur[gp_situaciones][]" value="<?= $valor['maestro_id'] ?>">
                        <tr> 
                            <td><?= $valor['descripcion'] ?></td>
                            <td><input type="date" class="form-control" name="Fur[gp_fechas_programadas][]" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                            <td><input type="date" class="form-control" name="Fur[gp_fechas_reales][]" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                            <?php if($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==0) { ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php }elseif($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==1){ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-danger btn-desactivar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Desactivar </button> </td>
                            <?php }else{ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                        <tr> 
                            <td>Descreto Supremo</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[gp_decreto_supremo]" value="<?= $model->gp_decreto_supremo ?>"></td>
                            <td></td>
                        </tr>
                        <tr> 
                            <td>Monto Transferido</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[gp_monto_transferido]" value="<?= $model->gp_monto_transferido ?>"></td>
                            <td></td>
                        </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<div class="col-md-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Expediente de técnico</h3>
            <span class="pull-right">
                <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
            </span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
            <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Fecha programada</th>
                        <th>Fecha real</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($model->lista_expediente_tecnico_fase_2 as $valor){ ?>
                        <input type="hidden" name="Fur[et_sub_etapas_ids][]" value="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>">
                        <input type="hidden" name="Fur[et_situaciones][]" value="<?= $valor['maestro_id'] ?>">
                        <tr> 
                            <td><?= $valor['descripcion'] ?></td>
                            <td><input type="date" name="Fur[et_fechas_programadas][]" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                            <td><input type="date" name="Fur[et_fechas_reales][]" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                            <?php if($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==0) { ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php }elseif($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==1){ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-danger btn-desactivar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Desactivar </button> </td>
                            <?php }else{ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                        <tr> 
                            <td>RD Aprobación</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[et_rd_aprobacion]" value="<?= $model->et_rd_aprobacion ?>"></td>
                            <td></td>
                        </tr>
                        <tr> 
                            <td>Monto de obra</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[et_monto_obra]" value="<?= $model->et_monto_obra ?>"></td>
                            <td></td>
                        </tr>
                        <tr> 
                            <td>Monto de supervisión de obra</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[et_monto_supervision_obra]" value="<?= $model->et_monto_supervision_obra ?>"></td>
                            <td></td>
                        </tr>
                        <tr> 
                            <td>Monto de expediente técnico</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[et_monto_expediente_tecnico]" value="<?= $model->et_monto_expediente_tecnico ?>"></td>
                            <td></td>
                        </tr>
                        <tr> 
                            <td>Monto de Supervisión de expediente</td>
                            <td colspan="2"><input type="text" class="form-control" name="Fur[et_monto_supervision_expediente]" value="<?= $model->et_monto_supervision_expediente ?>"></td>
                            <td></td>
                        </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<div class="clearfix"></div>
<div class="col-md-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Obra</h3>
            <span class="pull-right">
                <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
            </span>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
            <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Fecha programada</th>
                        <th>Fecha real</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($model->lista_obra_fase_2 as $valor){ ?>
                        <input type="hidden" name="Fur[o_sub_etapas_ids][]" value="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>">
                        <input type="hidden" name="Fur[o_situaciones][]" value="<?= $valor['maestro_id'] ?>">
                        <tr> 
                            <td><?= $valor['descripcion'] ?></td>
                            <td><input type="date" name="Fur[o_fechas_programadas][]" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                            <td><input type="date" name="Fur[o_fechas_reales][]" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                            <?php if($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==0) { ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php }elseif($model->fur_id && $valor['sub_etapa_id'] && $valor['estado']==1){ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-danger btn-desactivar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Desactivar </button> </td>
                            <?php }else{ ?>
                            <td><button data-sub-etapa-id="<?= ($model->fur_id && $valor['sub_etapa_id'])?$valor['sub_etapa_id']:''; ?>" class="btn btn-primary btn-activar" data-proyecto="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<div class="clearfix"></div>