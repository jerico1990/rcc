<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\P1fur */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Fur
    <small><?= $model->denominacion_fur ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Fur</a></li>
        <li class="active">información</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Ficha informativa</a></li>
                <?php if($model->codigo_fur){ ?>
                <li><a href="#tab_4" data-toggle="tab">Asignación de Intervenciones</a></li>
                <?php } ?>
                <li><a href="#tab_2" data-toggle="tab">Estudios</a></li>
                <li><a href="#tab_3" data-toggle="tab">Ejecución</a></li>
                <?php if(Yii::$app->user->identity->monitor!=1){ ?>
                <li class="pull-right">
                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'codigo_fur')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'denominacion_fur')->textarea(['rows' => 3])->label('Denominación FUR') ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'region_id')->dropDownList([],['prompt'=>'Seleccionar región'])->label('Región') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'provincia_id')->dropDownList([],['prompt'=>'Seleccionar provincia'])->label('Provincia') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'ubigeo_id')->dropDownList([],['prompt'=>'Seleccionar distrito'])->label('Distrito') ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'coordenada_latitud')->textInput(['maxlength' => true])->label('Coordenada Latitud') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'coordenada_longitud')->textInput(['maxlength' => true])->label('Coordenada Longitud') ?>
                    </div>
                    <!--
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_inicio_proyecto')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_fin_proyecto')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_inicio_ejecucion')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha_fin_ejecucion')->textInput(['maxlength' => true,'type'=>'date']) ?>
                    </div>
                    -->
                    <div class="clearfix"></div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'nro_hectareas')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'nro_familias')->textInput(['maxlength' => true]) ?>
                    </div>

                    

                    <div class="clearfix"></div>
                </div>

                <div class="tab-pane" id="tab_4">
                    
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-lista-intervenciones"> Agregar intervención</button>
                    </div>
                    <div class="clearfix"></div> <br>
                    <div class="col-md-12">
                        <table id="lista-intervenciones-asignadas" class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Código ARCC</th>
                                    <th>Intervención</th>
                                    <th>Total</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <div class="clearfix"></div>
                    <!--
                    <div class="col-md-4">
                        <?= $form->field($model, 'estudio_basico_fase')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Estudio Básico') ?>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'estudio_basico_modalidad_ejecucion_id')->dropDownList($model->lista_modalidad_ejecucion_estudio_basico,['prompt'=>'Seleccionar modalidad'])->label('Modalidad de ejecución') ?>
                    </div>
                    
                    <div class="clearfix"></div>

                    <div class="cargar_estados_fase_1" data-id="<?= $model->fur_id ?>"></div>

                    <?php /*
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Gestión presupuestal</h3>
                                <span class="pull-right">
                                    <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-gestion-presupuestal-estudio-basico" ><i class="la la-plus"></i>Agregar gestión presupuestal</button>-->
                                </span>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!--<table id="lista-gestion-presupuestal-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
                                <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Fecha programada</th>
                                            <th>Fecha real</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($model->lista_gestion_presupuestal_fase_1 as $valor){ ?>
                                            <tr> 
                                                <td><?= $valor['descripcion'] ?></td>
                                                <td><input type="date" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                                                <td><input type="date" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                                                <td> <button class="btn btn-primary btn-finalizar" data-fur="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Proceso de selección</h3>
                                <span class="pull-right">
                                    <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-proceso-seleccion-estudio-basico" ><i class="la la-plus"></i>Agregar proceso de selección</button>-->
                                </span>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!--<table id="lista-proceso-seleccion-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
                                <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Fecha programada</th>
                                            <th>Fecha real</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($model->lista_proceso_seleccion_fase_1 as $valor){ ?>
                                            <tr> 
                                                <td><?= $valor['descripcion'] ?></td>
                                                <td><input type="date" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                                                <td><input type="date" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                                                <td> <button class="btn btn-primary btn-finalizar" data-fur="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Expediente de técnico</h3>
                                <span class="pull-right">
                                    <!--<button data-id="<?= $model->fur_id ?>" class="btn btn-primary btn-agregar-expediente-tecnico-estudio-basico" ><i class="la la-plus"></i>Agregar expediente técnico</button>-->
                                </span>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!--<table id="lista-expediente-tecnico-fase1" class="table table-striped table-bordered responsive display nowrap" style="width:100%">-->
                                <table class="table table-striped table-bordered responsive display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Descripcion</th>
                                            <th>Fecha programada</th>
                                            <th>Fecha real</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($model->lista_expediente_tecnico_fase_1 as $valor){ ?>
                                            <tr> 
                                                <td><?= $valor['descripcion'] ?></td>
                                                <td><input type="date" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_programada']:''; ?>" > </td>
                                                <td><input type="date" class="form-control" value="<?= ($model->fur_id)?$valor['fecha_real']:''; ?>" ></td>
                                                <td> <button class="btn btn-primary btn-finalizar" data-fur="<?= $model->fur_id ?>" data-estado="<?= $valor['maestro_id']  ?>" > Activar </button> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    */ ?>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                <div class="clearfix"></div>
                    <!--
                    <div class="col-md-4">
                        <?= $form->field($model, 'ejecucion_fase')->checkBox(['label' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'])->label('Ejecución') ?>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'ejecucion_modalidad_ejecucion_id')->dropDownList($model->lista_modalidad_ejecucion_obra,['prompt'=>'Seleccionar modalidad'])->label('Modalidad de ejecución') ?>
                    </div>
                    <div class="cargar_estados_fase_2" data-id="<?= $model->fur_id ?>"></div>
                    
                    
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->
    <?php ActiveForm::end(); ?>
</section>
<!-- /.content -->
<div id="modal" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";



var fur_id = '<?= $model->fur_id ?>';
var modalidad_id_fase_2 = '<?= $model->ejecucion_modalidad_ejecucion_id ?>';


$('body').on('change', '#fur-ejecucion_modalidad_ejecucion_id', function (e) {
    e.preventDefault();
    fur_id = $('.cargar_estados_fase_2').attr('data-id');
    modalidad_id_fase_2 = $(this).val();
    CargaEstadosFase2(modalidad_id_fase_2,fur_id)
});

CargaEstadosFase2(modalidad_id_fase_2,fur_id)
function CargaEstadosFase2(modalidad_id_fase_2,fur_id){
    if(modalidad_id_fase_2){
        $('.cargar_estados_fase_2').load('<?= \Yii::$app->request->BaseUrl ?>/fur/cargar-etapas-fase2?fur_id=' + fur_id + '&fase_id=2&modalidad_id='+ modalidad_id_fase_2 +'');
    }
}

var modalidad_id_fase_1 = '<?= $model->estudio_basico_modalidad_ejecucion_id ?>';

$('body').on('change', '#fur-estudio_basico_modalidad_ejecucion_id', function (e) {
    e.preventDefault();
    fur_id = $('.cargar_estados_fase_1').attr('data-id');
    modalidad_id_fase_1 = $(this).val();
    CargaEstadosFase1(modalidad_id_fase_1,fur_id)
});


CargaEstadosFase1(modalidad_id_fase_1,fur_id)
function CargaEstadosFase1(modalidad_id_fase_1,fur_id){
    if(modalidad_id_fase_1){
        $('.cargar_estados_fase_1').load('<?= \Yii::$app->request->BaseUrl ?>/fur/cargar-etapas-fase1?fur_id=' + fur_id + '&fase_id=1&modalidad_id='+ modalidad_id_fase_1 +'');
    }
}




$('body').on('click', '.btn-lista-intervenciones', function (e) {
    e.preventDefault();
    var fur_id='<?= $model->fur_id ?>';
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/intervenciones/lista?fur_id='+fur_id);
    $('#modal').modal('show');
});

CargarIntervenciones();
function CargarIntervenciones(){
    var fur_id= '<?= $model->fur_id ?>';
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/fur/get-lista-intervenciones',
        method: 'POST',
        data:{_csrf:csrf,fur_id:fur_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                var tabla = '';
                var total_monto_base=0.00;
                $.each(results.data, function( index, value ) {
                    cnombre = value.intervencion;
                    tabla = tabla + '<tr>';
                        tabla = tabla + '<td>' ;   
                            tabla = tabla + value.codigo_arcc;
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;   
                            tabla = tabla + ((cnombre)?cnombre.substring(0,50):'');
                        tabla = tabla + '</td>' ;
                        tabla = tabla + '<td>' ;   
                            tabla = tabla + number_format(value.monto_base,2);
                        tabla = tabla + '</td>' ;

                        tabla = tabla + '<td>' ;
                            tabla = tabla + '<span data-id="' +value.proyecto_id + '" class="btn btn-danger btn-sm fa fa-minus btn-quitar-intervencion"></span>' ; 
                        tabla = tabla + '</td>' ;
                    tabla = tabla + '</tr>';
                    total_monto_base = total_monto_base + parseFloat(value.monto_base);
                });

                tabla = tabla + '<tr>';
                    tabla = tabla + '<td>' ;   
                        tabla = tabla + '';
                    tabla = tabla + '</td>' ;
                    tabla = tabla + '<td>' ;   
                        tabla = tabla + 'Total';
                    tabla = tabla + '</td>' ;
                    tabla = tabla + '<td>' ;   
                        tabla = tabla + number_format(total_monto_base,2);
                    tabla = tabla + '</td>' ;

                    tabla = tabla + '<td>' ;
                        
                    tabla = tabla + '</td>' ;
                tabla = tabla + '</tr>';


                $('#lista-intervenciones-asignadas tbody').html(tabla);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}


$('body').on('click', '.btn-quitar-intervencion', function (e) {
    e.preventDefault();
    var intervencion_id= $(this).attr('data-id');
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/fur/quitar-asignacion-intervencion',
        method: 'POST',
        data:{_csrf:csrf,fur_id:fur_id,intervencion_id:intervencion_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                CargarIntervenciones();
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});


$('body').on('click', '.btn-activar', function (e) {
    e.preventDefault();
    var sub_etapa_id= $(this).attr('data-sub-etapa-id');
    var $this = $(this);
    if(sub_etapa_id==''){
        alert('Deberá guardar previamente la modalidad de ejecución');
        return false;
    }
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/fur/activar-sub-etapa',
        method: 'POST',
        data:{_csrf:csrf,sub_etapa_id:sub_etapa_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                $this.html('Desactivar');
                $this.removeClass('btn-primary');
                $this.removeClass('btn-activar');
                $this.addClass('btn-danger');
                $this.addClass('btn-desactivar');
                //alert('Se ha p')
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});

$('body').on('click', '.btn-desactivar', function (e) {
    e.preventDefault();
    var sub_etapa_id= $(this).attr('data-sub-etapa-id');
    var $this = $(this);
    if(sub_etapa_id==''){
        alert('Deberá guardar previamente la modalidad de ejecución');
        return false;
    }
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/fur/desactivar-sub-etapa',
        method: 'POST',
        data:{_csrf:csrf,sub_etapa_id:sub_etapa_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                $this.html('Activar');
                $this.removeClass('btn-danger');
                $this.removeClass('btn-desactivar');
                $this.addClass('btn-primary');
                $this.addClass('btn-activar');
                //alert('Se ha p')
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
});



function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    console.log(amount);
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}


var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var ubigeo = '<?= ($model->ubigeo_id)?$model->ubigeo_id:''; ?>';
var region = (ubigeo)?ubigeo.substring(0,2):'';
var provincia = (ubigeo)?ubigeo.substring(0,4):'';
var distrito = (ubigeo)?ubigeo.substring(0,6):'';
Region();
async function Region(){
    region = (ubigeo)?ubigeo.substring(0,2):'';
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-regiones',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            console.log(region);
            if(results && results.success){
                var option='<option value>Seleccionar región</option>';
                //data.push({'id':'0','text':'Seleccionar región','selected':true});
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((region == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#fur-region_id').html(option);
            }
            
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}

$('body').on('change', '#fur-region_id', function (e) {
    e.preventDefault();
    region = $('#fur-region_id').val();
    provincia = $('#fur-provincia_id').val();
    $('#fur-ubigeo_id').html('<option value>Seleccionar distrito</option>');
    Provincia();
});


Provincia();
async function Provincia(){
    //var region = (ubigeo)?ubigeo.substring(0,2):$('#p1proyecto-region_id').val();
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-provincias',
        method: 'POST',
        data:{_csrf:csrf,region:region},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                var option = '<option value>Seleccionar provincia</option>';
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((provincia == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#fur-provincia_id').html(option);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}


$('body').on('change', '#fur-provincia_id', function (e) {
    e.preventDefault();
    provincia = $('#fur-provincia_id').val();
    distrito = $('#fur-ubigeo_id').val();
    Distrito();
});


Distrito();
function Distrito(){
    //var provincia = (ubigeo)?ubigeo.substring(0,4):$('#p1proyecto-provincia_id').val();
    //var distrito = (ubigeo)?ubigeo.substring(0,6):$('#p1proyecto-provincia_id').val();
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-lista-distritos',
        method: 'POST',
        data:{_csrf:csrf,provincia:provincia},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //$("#p1proyecto-ubigeo_id").html('<option value>Seleccionar distrito</option>');
                //$("#p1proyecto-provincia_id").html('<option value>Seleccionar provincia</option>');
                var option = '<option value>Seleccionar distrito</option>';
                $( results.data ).each(function( index ,value ) {
                    option = option + '<option value="' + value.id + '" '+((distrito == value.id)?'selected':'')+'>' + value.text + '</option>';
                });
                $('#fur-ubigeo_id').html(option);
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}



</script>