<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GestionPresupuestal */
?>
<div class="gestion-presupuestal-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'gestion_presupuestal_id',
            'tipo_id',
            'fase_id',
            'fecha_programada',
            'fecha_real',
        ],
    ]) ?>

</div>
