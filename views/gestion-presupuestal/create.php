<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GestionPresupuestal */

?>
<div class="gestion-presupuestal-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
