<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GestionPresupuestal */
?>
<div class="gestion-presupuestal-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
