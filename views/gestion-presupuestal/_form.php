<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GestionPresupuestal */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="modal-header">
        <h5 class="modal-title"><?= $model->titulo ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['id' => 'formGestionPresupuestal']]); ?>
    <div class="modal-body">
        <?= $form->field($model, 'tipo_id')->dropDownList($model->lista_tipos,['prompt'=>'Seleccionar'])->label('Tipo') ?>

        <?= $form->field($model, 'fecha_programada')->textInput(['type'=>'date']) ?>

        <?= $form->field($model, 'fecha_real')->textInput(['type'=>'date']) ?>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn  btn-primary pull-right btn-grabar-gestion-presupuestal"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
  
	<?php /* if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php }*/ ?>

    <?php ActiveForm::end(); ?>

