<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>AdminLTE 2 | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/Ionicons/css/ionicons.min.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/jvectormap/jquery-jvectormap.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/select2/dist/css/select2.min.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
		folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/css/skins/_all-skins.min.css">

	<!-- DataTables -->
	<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



  	<!-- jQuery 3 -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/js/adminlte.min.js"></script>
	<!-- Sparkline -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Select2 -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/select2/dist/js/select2.full.min.js"></script>


	<!-- SlimScroll -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/chart.js/Chart.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/js/pages/dashboard2.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/js/demo.js"></script>

	<!-- DataTables -->
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

	<header class="main-header">

		<!-- Logo -->
		<a href="<?= \Yii::$app->request->BaseUrl ?>/panel" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>R</b>CC</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>R</b>CC</span>
		</a>

		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
			
				<!-- Notifications: style can be found in dropdown.less -->
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-bell-o"></i>
					<span class="label label-warning">10</span>
					</a>
					<ul class="dropdown-menu">
					<li class="header">You have 10 notifications</li>
					<li>
						<!-- inner menu: contains the actual data -->
						<ul class="menu">
						<li>
							<a href="#">
							<i class="fa fa-users text-aqua"></i> 5 new members joined today
							</a>
						</li>
						<li>
							<a href="#">
							<i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
							page and may cause design problems
							</a>
						</li>
						<li>
							<a href="#">
							<i class="fa fa-users text-red"></i> 5 new members joined
							</a>
						</li>
						<li>
							<a href="#">
							<i class="fa fa-shopping-cart text-green"></i> 25 sales made
							</a>
						</li>
						<li>
							<a href="#">
							<i class="fa fa-user text-red"></i> You changed your username
							</a>
						</li>
						</ul>
					</li>
					<li class="footer"><a href="#">View all</a></li>
					</ul>
				</li>
				<!-- Tasks: style can be found in dropdown.less -->
				
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<img src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
					<span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
					</a>
					<ul class="dropdown-menu">
					<!-- User image -->
					<li class="user-header">
						<img src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

						<p>
						Alexander Pierce - Web Developer
						<small>Member since Nov. 2012</small>
						</p>
					</li>
					<!-- Menu Body -->
					<li class="user-body">
						<div class="row">
						<div class="col-xs-4 text-center">
							<a href="#">Followers</a>
						</div>
						<div class="col-xs-4 text-center">
							<a href="#">Sales</a>
						</div>
						<div class="col-xs-4 text-center">
							<a href="#">Friends</a>
						</div>
						</div>
						<!-- /.row -->
					</li>
					<!-- Menu Footer-->
					<li class="user-footer">
						<div class="pull-left">
						<a href="#" class="btn btn-default btn-flat">Profile</a>
						</div>
						<div class="pull-right">
						<a href="<?= \Yii::$app->request->BaseUrl ?>/login/logout" class="btn btn-default btn-flat">Cerrar sesión</a>
						</div>
					</li>
					</ul>
				</li>
			</ul>
		</div>

		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
			<img src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-2.4.10/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
			<p>Alexander Pierce</p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li class="active treeview menu-open">
				<a href="#">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
					<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= \Yii::$app->request->BaseUrl ?>/panel/"><i class="fa fa-circle-o"></i> Panel</a></li>
					<li><a href="<?= \Yii::$app->request->BaseUrl ?>/fur/"><i class="fa fa-circle-o"></i> FUR</a></li>
				</ul>
			</li>
			<li class="treeview menu-open">
				<a href="#">
					<i class="fa fa-dashboard"></i> <span>Maestro</span>
					<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="active"><a href="<?= \Yii::$app->request->BaseUrl ?>/intervenciones/"><i class="fa fa-circle-o"></i> Intervenciones</a></li>
					<?php if(Yii::$app->user->identity->administrador==1){ ?>
					<li class="active"><a href="<?= \Yii::$app->request->BaseUrl ?>/usuarios/"><i class="fa fa-circle-o"></i> Usuarios</a></li>
					<?php } ?>
				</ul>
			</li>
			<li class="treeview menu-open">
				<a href="#">
					<i class="fa fa-dashboard"></i> <span>Reporte</span>
					<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/mapa"><i class="fa fa-circle-o"></i> Mapa</a></li>
				</ul>
			</li>
		</ul>
		</section>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<?= $content ?>
	</div>
	<!-- /.content-wrapper -->

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
		<b>Version</b> 1.0.0
		</div>
		<strong>Todos los derechos reservados - 2019</strong>
	</footer>

	<!-- Add the sidebar's background. This div must be placed
		immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->




</body>
</html>
