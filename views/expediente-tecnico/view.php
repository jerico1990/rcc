<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ExpedienteTecnico */
?>
<div class="expediente-tecnico-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'expediente_tecnico_id',
            'tipo_id',
            'fase_id',
            'fecha_programada',
            'fecha_real',
            'proyecto_id',
        ],
    ]) ?>

</div>
