<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExpedienteTecnico */

?>
<div class="expediente-tecnico-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
