<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProcesoSeleccion */
?>
<div class="proceso-seleccion-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
