<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProcesoSeleccion */
?>
<div class="proceso-seleccion-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'proceso_seleccion_id',
            'tipo_id',
            'fase_id',
            'fecha_programada',
            'fecha_real',
            'proyecto_id',
        ],
    ]) ?>

</div>
