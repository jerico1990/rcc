<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gestion_presupuestal".
 *
 * @property int $gestion_presupuestal_id
 * @property int $tipo_id
 * @property int $fase_id
 * @property string $fecha_programada
 * @property string $fecha_real
 */
class GestionPresupuestal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public $lista_tipos;
    public static function tableName()
    {
        return 'gestion_presupuestal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_id', 'fase_id'], 'integer'],
            [['fecha_programada', 'fecha_real'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'gestion_presupuestal_id' => 'Gestion Presupuestal ID',
            'tipo_id' => 'Tipo ID',
            'fase_id' => 'Fase ID',
            'fecha_programada' => 'Fecha Programada',
            'fecha_real' => 'Fecha Real',
        ];
    }
}
