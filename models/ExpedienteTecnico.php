<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expediente_tecnico".
 *
 * @property int $expediente_tecnico_id
 * @property int $tipo_id
 * @property int $fase_id
 * @property string $fecha_programada
 * @property string $fecha_real
 * @property int $proyecto_id
 */
class ExpedienteTecnico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $lista_tipos;
    public $titulo;
    public static function tableName()
    {
        return 'expediente_tecnico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_id', 'fase_id', 'proyecto_id'], 'integer'],
            [['fecha_programada', 'fecha_real'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'expediente_tecnico_id' => 'Expediente Tecnico ID',
            'tipo_id' => 'Tipo ID',
            'fase_id' => 'Fase ID',
            'fecha_programada' => 'Fecha Programada',
            'fecha_real' => 'Fecha Real',
            'proyecto_id' => 'Proyecto ID',
        ];
    }
}
