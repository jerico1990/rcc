<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fur".
 *
 * @property int $fur_id
 * @property string $codigo_fur
 * @property int $estudio_basico_fase
 * @property int $ejecucion_fase
 * @property int $estudio_basico_modalidad_ejecucion_id
 * @property int $ejecucion_modalidad_ejecucion_id
 * @property int $nro_hectareas
 * @property int $nro_familias
 * @property string $fecha_inicio_proyecto
 * @property string $fecha_inicio_ejecucion
 * @property string $fecha_fin_ejecucion
 * @property string $fecha_fin_proyecto
 * @property string $finalidad
 */
class Fur extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $lista_modalidad_ejecucion_estudio_basico;
    public $lista_modalidad_ejecucion_obra;

    public $lista_gestion_presupuestal_fase_1;
    public $lista_proceso_seleccion_fase_1;
    public $lista_expediente_tecnico_fase_1;
    public $lista_eib_fase_1;

    public $lista_gestion_presupuestal_fase_2;
    public $lista_proceso_seleccion_fase_2;
    public $lista_expediente_tecnico_fase_2;
    public $lista_obra_fase_2;
    public $lista_operatividad_ner_fase_2;

    public $et1_sub_etapas_ids;
    public $et1_situaciones;
    public $et1_fechas_programadas;
    public $et1_fechas_reales;

    public $eib_sub_etapas_ids;
    public $eib_situaciones;
    public $eib_fechas_programadas;
    public $eib_fechas_reales;

    public $gp_sub_etapas_ids;
    public $gp_situaciones;
    public $gp_fechas_programadas;
    public $gp_fechas_reales;

    public $ps_sub_etapas_ids;
    public $ps_situaciones;
    public $ps_fechas_programadas;
    public $ps_fechas_reales;

    public $ner_sub_etapas_ids;
    public $ner_situaciones;
    public $ner_fechas_programadas;
    public $ner_fechas_reales;

    public $et_sub_etapas_ids;
    public $et_situaciones;
    public $et_fechas_programadas;
    public $et_fechas_reales;

    public $o_sub_etapas_ids;
    public $o_situaciones;
    public $o_fechas_programadas;
    public $o_fechas_reales;

    public $region_lista;
    public $provincia_lista;
    public $distrito_lista;
    public $region_id;
    public $provincia_id;

    public static function tableName()
    {
        return 'fur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_fur','denominacion_fur'],'required'],
            [['ubigeo_id'], 'string', 'max' => 6],
            [['estudio_basico_fase', 'ejecucion_fase', 'estudio_basico_modalidad_ejecucion_id', 'ejecucion_modalidad_ejecucion_id', 'nro_hectareas', 'nro_familias'], 'integer'],
            [['ps_correo_electronico','ps_contraparte','ner_monto_convenio','ner_nombre_convenio','eib_sub_etapas_ids','eib_situaciones','eib_fechas_programadas','eib_fechas_reales','et1_sub_etapas_ids','et1_situaciones','et1_fechas_programadas','et1_fechas_reales','et_monto_supervision_expediente','et_monto_expediente_tecnico','et_monto_supervision_obra','et_monto_obra','et_rd_aprobacion','ps_monto_adjudicado','ps_nro_contrato','gp_monto_transferido','gp_decreto_supremo','ner_sub_etapas_ids','ner_situaciones','ner_fechas_programadas','ner_fechas_reales','o_sub_etapas_ids','o_situaciones','o_fechas_programadas','o_fechas_reales','et_sub_etapas_ids','et_situaciones','et_fechas_programadas','et_fechas_reales','ps_sub_etapas_ids','ps_situaciones','ps_fechas_programadas','ps_fechas_reales','gp_sub_etapas_ids','gp_situaciones','gp_fechas_programadas','gp_fechas_reales','fecha_inicio_proyecto', 'fecha_inicio_ejecucion', 'fecha_fin_ejecucion', 'fecha_fin_proyecto'], 'safe'],
            [['denominacion_fur','coordenada_latitud','coordenada_longitud'], 'string'],
            [['codigo_fur'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fur_id' => 'Fur ID',
            'codigo_fur' => 'Codigo Fur',
            'estudio_basico_fase' => 'Estudio Basico Fase',
            'ejecucion_fase' => 'Ejecucion Fase',
            'estudio_basico_modalidad_ejecucion_id' => 'Estudio Basico Modalidad Ejecucion ID',
            'ejecucion_modalidad_ejecucion_id' => 'Ejecucion Modalidad Ejecucion ID',
            'nro_hectareas' => 'Nro Hectareas',
            'nro_familias' => 'Nro Familias',
            'fecha_inicio_proyecto' => 'Fecha Inicio Proyecto',
            'fecha_inicio_ejecucion' => 'Fecha Inicio Ejecucion',
            'fecha_fin_ejecucion' => 'Fecha Fin Ejecucion',
            'fecha_fin_proyecto' => 'Fecha Fin Proyecto',
            'denominacion_fur' => 'Finalidad',
        ];
    }
}
