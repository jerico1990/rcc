<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GestionPresupuestal;

/**
 * GestionPresupuestalSearch represents the model behind the search form about `app\models\GestionPresupuestal`.
 */
class GestionPresupuestalSearch extends GestionPresupuestal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gestion_presupuestal_id', 'tipo_id', 'fase_id'], 'integer'],
            [['fecha_programada', 'fecha_real'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GestionPresupuestal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gestion_presupuestal_id' => $this->gestion_presupuestal_id,
            'tipo_id' => $this->tipo_id,
            'fase_id' => $this->fase_id,
            'fecha_programada' => $this->fecha_programada,
            'fecha_real' => $this->fecha_real,
        ]);

        return $dataProvider;
    }
}
