<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "p2_persona".
 *
 * @property int $idperson
 * @property string $capepat
 * @property string $capemat
 * @property string $cnombre
 * @property string $ccorreo
 * @property string $ccontra
 * @property int $besusua
 * @property int $beditor
 */
class P2Persona extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public $auth_key;
    public $contrasena;
    public static function tableName()
    {
        return 'p2_persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ccorreo'],'required'],
            [['badministrador', 'bmonitor','bejecutoras'], 'integer'],
            [['capepat', 'capemat', 'cnombre'], 'string', 'max' => 50],
            [['ccorreo', 'ccontra','contrasena'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idperson' => 'Idperson',
            'capepat' => 'Capepat',
            'capemat' => 'Capemat',
            'cnombre' => 'Cnombre',
            'ccorreo' => 'Ccorreo',
            'ccontra' => 'Ccontra',
        ];
    }


    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getUsername(){
        return $this->ccorreo;
    }


    public static function findByUsername($password,$username)
    {
      return static::find()->where('ccorreo=:ccorreo and bestado=1',[':ccorreo' => $username])->one();
    }

    public function validatePassword($password,$username){

        $model=static::find()->where('ccorreo=:ccorreo and bestado=1',[':ccorreo' => $username])->one();
        if(Yii::$app->getSecurity()->validatePassword($password, $model->ccontra))
        {
            return $model;
        }
        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null){
        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

    public function getAdministrador(){
        return $this->badministrador;
    }

    public function getMonitor(){
        return $this->bmonitor;
    }

    public function getEjecutoras(){
        return $this->bejecutoras;
    }

    public function PersonasEjecutorasArrayHelper(){
        $personas=(new \yii\db\Query())
        ->select('*')
        ->from('p2_persona')
        ->where('bejecutoras=:bejecutoras',[':bejecutoras'=>1])
        ->all();
        return ArrayHelper::map($personas,'idperson','cnombre');
    }
}
