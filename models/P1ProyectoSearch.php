<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\P1Proyecto;

/**
 * P1ProyectoSearch represents the model behind the search form about `app\models\P1Proyecto`.
 */
class P1ProyectoSearch extends P1Proyecto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proyecto_id'], 'integer'],
            [['codigo_arcc', 'ubigeo_id', 'intervencion', 'decreto_supremo'], 'safe'],
            [['monto_base'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = P1Proyecto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'proyecto_id' => $this->proyecto_id,
            'monto_base' => $this->monto_base,
        ]);

        $query->andFilterWhere(['like', 'codigo_arcc', $this->codigo_arcc])
            ->andFilterWhere(['like', 'ubigeo_id', $this->ubigeo_id])
            ->andFilterWhere(['like', 'intervencion', $this->intervencion])
            ->andFilterWhere(['like', 'decreto_supremo', $this->decreto_supremo]);

        return $dataProvider;
    }
}
