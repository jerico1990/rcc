<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fur;

/**
 * FurSearch represents the model behind the search form about `app\models\Fur`.
 */
class FurSearch extends Fur
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fur_id', 'estudio_basico_fase', 'ejecucion_fase', 'estudio_basico_modalidad_ejecucion_id', 'ejecucion_modalidad_ejecucion_id', 'nro_hectareas', 'nro_familias'], 'integer'],
            [['codigo_fur', 'fecha_inicio_proyecto', 'fecha_inicio_ejecucion', 'fecha_fin_ejecucion', 'fecha_fin_proyecto', 'denominacion_fur'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fur::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fur_id' => $this->fur_id,
            'estudio_basico_fase' => $this->estudio_basico_fase,
            'ejecucion_fase' => $this->ejecucion_fase,
            'estudio_basico_modalidad_ejecucion_id' => $this->estudio_basico_modalidad_ejecucion_id,
            'ejecucion_modalidad_ejecucion_id' => $this->ejecucion_modalidad_ejecucion_id,
            'nro_hectareas' => $this->nro_hectareas,
            'nro_familias' => $this->nro_familias,
            'fecha_inicio_proyecto' => $this->fecha_inicio_proyecto,
            'fecha_inicio_ejecucion' => $this->fecha_inicio_ejecucion,
            'fecha_fin_ejecucion' => $this->fecha_fin_ejecucion,
            'fecha_fin_proyecto' => $this->fecha_fin_proyecto,
        ]);

        $query->andFilterWhere(['like', 'codigo_fur', $this->codigo_fur]);

        return $dataProvider;
    }
}
