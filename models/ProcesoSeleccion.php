<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proceso_seleccion".
 *
 * @property int $proceso_seleccion_id
 * @property int $tipo_id
 * @property int $fase_id
 * @property string $fecha_programada
 * @property string $fecha_real
 * @property int $proyecto_id
 */
class ProcesoSeleccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $lista_tipos;
    public $titulo;
    public static function tableName()
    {
        return 'proceso_seleccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_id', 'fase_id', 'proyecto_id'], 'integer'],
            [['fecha_programada', 'fecha_real'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'proceso_seleccion_id' => 'Proceso Seleccion ID',
            'tipo_id' => 'Tipo ID',
            'fase_id' => 'Fase ID',
            'fecha_programada' => 'Fecha Programada',
            'fecha_real' => 'Fecha Real',
            'proyecto_id' => 'Proyecto ID',
        ];
    }
}
