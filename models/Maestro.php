<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "maestro".
 *
 * @property int $maestro_id
 * @property string $descripcion
 * @property int $padre_id
 * @property string $orden
 */
class Maestro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maestro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['padre_id'], 'integer'],
            [['descripcion', 'orden'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'maestro_id' => 'Maestro ID',
            'descripcion' => 'Descripcion',
            'padre_id' => 'Padre ID',
            'orden' => 'Orden',
        ];
    }

    public function MaestroArrayHelper($padre_id=null){
        $maestro=(new \yii\db\Query())
        ->select('*')
        ->from('maestro')
        ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
        ->all();

        return ArrayHelper::map($maestro,'maestro_id','descripcion');
    }

    public function MaestroArraySubEtapas($fur_id=null,$fase_id=null,$modalidad_id=null,$etapa_id=null){
        if($fur_id){
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->leftJoin('sub_etapa','sub_etapa.fur_id='.$fur_id.' and sub_etapa.fase_id='.$fase_id.' and sub_etapa.modalidad_id='.$modalidad_id.' and sub_etapa.etapa_id='.$etapa_id.' and sub_etapa.situacion_id=maestro.maestro_id')
            ->where('padre_id=:padre_id',[':padre_id'=>$etapa_id])
            ->all();
        }else{
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->where('padre_id=:padre_id',[':padre_id'=>$etapa_id])
            ->all();
        }

        return $maestro;
    }

    public function MaestroArrayGestionPresupuestal($padre_id=null,$fase_id=null,$fur_id=null){
        if($fur_id){
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->leftJoin('gestion_presupuestal','gestion_presupuestal.tipo_id=maestro.maestro_id and fase_id='.$fase_id.' and fur_id='.$fur_id.'')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }else{
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }

        return $maestro;
    }

    public function MaestroArrayProcesoSeleccion($padre_id=null,$fase_id=null,$fur_id=null){
        if($fur_id){
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->leftJoin('proceso_seleccion','proceso_seleccion.tipo_id=maestro.maestro_id and fase_id='.$fase_id.' and fur_id='.$fur_id.'')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }else{
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }

        return $maestro;
    }

    public function MaestroArrayExpedienteTecnico($padre_id=null,$fase_id=null,$fur_id=null){
        if($fur_id){
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->leftJoin('expediente_tecnico','expediente_tecnico.tipo_id=maestro.maestro_id and fase_id='.$fase_id.' and fur_id='.$fur_id.'')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }else{
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }

        return $maestro;
    }

    public function MaestroArrayObra($padre_id=null,$fase_id=null,$fur_id=null){
        if($fur_id){
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->leftJoin('obra','obra.tipo_id=maestro.maestro_id and fase_id='.$fase_id.' and fur_id='.$fur_id.'')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }else{
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }

        return $maestro;
    }

    public function MaestroArrayOperatividadNER($padre_id=null,$fase_id=null,$fur_id=null){
        if($fur_id){
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->leftJoin('operatividad_ner','operatividad_ner.tipo_id=maestro.maestro_id and fase_id='.$fase_id.' and fur_id='.$fur_id.'')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }else{
            $maestro=(new \yii\db\Query())
            ->select('*')
            ->from('maestro')
            ->where('padre_id=:padre_id',[':padre_id'=>$padre_id])
            ->all();
        }

        return $maestro;
    }
}
