<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_etapa".
 *
 * @property int $sub_etapa_id
 * @property int $fur_id
 * @property int $fase_id
 * @property int $modalidad_id
 * @property int $etapa_id
 * @property int $situacion_id
 * @property string $fecha_programada
 * @property string $fecha_real
 * @property int $estado
 */
class SubEtapa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_etapa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fur_id', 'fase_id', 'modalidad_id', 'etapa_id', 'situacion_id', 'estado'], 'integer'],
            [['fecha_programada', 'fecha_real'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sub_etapa_id' => 'Sub Etapa ID',
            'fur_id' => 'Fur ID',
            'fase_id' => 'Fase ID',
            'modalidad_id' => 'Modalidad ID',
            'etapa_id' => 'Etapa ID',
            'situacion_id' => 'Situacion ID',
            'fecha_programada' => 'Fecha Programada',
            'fecha_real' => 'Fecha Real',
            'estado' => 'Estado',
        ];
    }
}
