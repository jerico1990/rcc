<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "ubigeo".
 *
 * @property string $id
 * @property string $nombre
 */
class Ubigeo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubigeo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['id', 'nombre'], 'required'],
            [['id'], 'string', 'max' => 6],
            [['nombre'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    public function UbigeoArrayHelper($tipo){
        $ubigeos=(new \yii\db\Query())
        ->select('*')
        ->from('ubigeo')
        ->where('LENGTH(ubigeo.id)=:id',[':id'=>$tipo])
        ->all();
        return ArrayHelper::map($ubigeos,'id','nombre');
    }
}
