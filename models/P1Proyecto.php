<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "p1_proyecto".
 *
 * @property int $proyecto_id
 * @property string $codigo_arcc
 * @property string $ubigeo_id
 * @property string $intervencion
 * @property double $monto_base
 * @property string $decreto_supremo
 */
class P1Proyecto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'p1_proyecto';
    }

    /**
     * {@inheritdoc}
     */
    public $region_lista;
    public $provincia_lista;
    public $distrito_lista;
    public $region_id;
    public $provincia_id;
    public $asignar_proyecto_personas;
    public $lista_modalidad_ejecucion_estudio_basico;
    public $lista_modalidad_ejecucion_obra;

    public $lista_gestion_presupuestal_fase_1;
    public $lista_proceso_seleccion_fase_1;
    public $lista_expediente_tecnico_fase_1;

    public $lista_gestion_presupuestal_fase_2;
    public $lista_proceso_seleccion_fase_2;
    public $lista_expediente_tecnico_fase_2;
    public $lista_obra_fase_2;
    public $lista_operatividad_ner_fase_2;

    public function rules()
    {
        return [
            [['monto_base'], 'number'],
            [['codigo_arcc'], 'string', 'max' => 50],
            [['ubigeo_id'], 'string', 'max' => 6],
            [['intervencion'], 'string', 'max' => 250],
            [['decreto_supremo'], 'string', 'max' => 150],
            [['asignar_proyecto_personas','estudio_basico_fase','ejecucion_fase','estudio_basico_modalidad_ejecucion_id','ejecucion_modalidad_ejecucion_id'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'proyecto_id' => 'Proyecto ID',
            'codigo_arcc' => 'Codigo Arcc',
            'ubigeo_id' => 'Ubigeo ID',
            'intervencion' => 'Intervencion',
            'monto_base' => 'Monto Base',
            'decreto_supremo' => 'Decreto Supremo',
        ];
    }
}
