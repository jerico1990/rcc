<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asignacion".
 *
 * @property int $p2_persona_id
 * @property int $p1_proyecto_id
 */
class Asignacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asignacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p2_persona_id', 'p1_proyecto_id'], 'required'],
            [['p2_persona_id', 'p1_proyecto_id'], 'integer'],
            [['p2_persona_id', 'p1_proyecto_id'], 'unique', 'targetAttribute' => ['p2_persona_id', 'p1_proyecto_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'p2_persona_id' => 'P2 Persona ID',
            'p1_proyecto_id' => 'P1 Proyecto ID',
        ];
    }
}
